const Stack = createNativeStackNavigator();
import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { useFonts } from "expo-font";
import HOME from "./screens/HOME";
import Notifications from "./screens/Notifications";
import ChatPage from "./screens/ChatPage";
import ChatPage1 from "./screens/ChatPage1";
import ChatPage2 from "./screens/ChatPage2";
import NewsPromo from "./screens/NewsPromo";
import ActivityHistory from "./screens/ActivityHistory";
import Chat from "./screens/Chat";
import SelectCountryRegion from "./components/SelectCountryRegion";
import EditProfile from "./screens/EditProfile";
import Membership from "./screens/Membership";
import Account from "./components/Account";
import Property1Default from "./components/Property1Default";
import ActiveItemhome from "./components/ActiveItemhome";

import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { View, Text, Pressable, TouchableOpacity } from "react-native";

const App = () => {
  const [hideSplashScreen, setHideSplashScreen] = React.useState(true);
  const [fontsLoaded, error] = useFonts({
    "Roboto-Regular": require("./assets/fonts/Roboto-Regular.ttf"),
    "Roboto-Medium": require("./assets/fonts/Roboto-Medium.ttf"),
    "Roboto-Bold": require("./assets/fonts/Roboto-Bold.ttf"),
    "Roboto-Black": require("./assets/fonts/Roboto-Black.ttf"),
    "Inter-Regular": require("./assets/fonts/Inter-Regular.ttf"),
    "Inter-Medium": require("./assets/fonts/Inter-Medium.ttf"),
    "Inter-SemiBold": require("./assets/fonts/Inter-SemiBold.ttf"),
    "Inter-Bold": require("./assets/fonts/Inter-Bold.ttf"),
    "Inter-Black": require("./assets/fonts/Inter-Black.ttf"),
  });

  if (!fontsLoaded && !error) {
    return null;
  }

  return (
    <>
      <NavigationContainer>
        {hideSplashScreen ? (
          <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen
              name="HOME"
              component={HOME}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Notifications"
              component={Notifications}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="ChatPage"
              component={ChatPage}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="ChatPage1"
              component={ChatPage1}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="ChatPage2"
              component={ChatPage2}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="NewsPromo"
              component={NewsPromo}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="ActivityHistory"
              component={ActivityHistory}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Chat"
              component={Chat}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="SelectCountryRegion"
              component={SelectCountryRegion}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="EditProfile"
              component={EditProfile}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Membership"
              component={Membership}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Account"
              component={Account}
              options={{ headerShown: false }}
            />
          </Stack.Navigator>
        ) : null}
      </NavigationContainer>
    </>
  );
};
export default App;
