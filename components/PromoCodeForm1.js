import * as React from "react";
import { StyleSheet, View, Text } from "react-native";
import { Image } from "expo-image";
import Property1Default from "./Property1Default";
import { Border, FontSize, FontFamily, Color } from "../GlobalStyles";

const PromoCodeForm1 = () => {
  return (
    <View style={[styles.frame, styles.framePosition]}>
      <Property1Default
        property1DefaultPosition="absolute"
        property1DefaultMarginLeft={-180}
        property1DefaultTop={0}
        property1DefaultLeft="50%"
        property1DefaultWidth={360}
      />
      <View style={[styles.gotPromoCodeParent, styles.frameIconLayout]}>
        <Text style={styles.gotPromoCode}>Got promo code?</Text>
        <Image
          style={[styles.frameIcon, styles.frameIconLayout]}
          contentFit="cover"
          source={require("../assets/frame1.png")}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  framePosition: {
    overflow: "hidden",
    top: 0,
  },
  frameIconLayout: {
    height: 42,
    borderRadius: Border.br_5xs,
    position: "absolute",
  },
  gotPromoCode: {
    top: 14,
    left: 17,
    fontSize: FontSize.headingH5_size,
    fontFamily: FontFamily.gBoardLarge,
    color: Color.colorGray_200,
    textAlign: "left",
    position: "absolute",
  },
  frameIcon: {
    left: 260,
    width: 42,
    overflow: "hidden",
    top: 0,
  },
  gotPromoCodeParent: {
    top: 270,
    left: 29,
    borderStyle: "solid",
    borderColor: Color.colorWhitesmoke_400,
    borderWidth: 1,
    width: 302,
  },
  frame: {
    left: 0,
    width: 360,
    height: 312,
    position: "absolute",
  },
});

export default PromoCodeForm1;
