import React, { useMemo } from "react";
import { Image } from "expo-image";
import { StyleSheet, Text, View, ImageSourcePropType, Pressable } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Padding, Border, FontSize, FontFamily, Color } from "../GlobalStyles";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};

const ActiveItemhome = ({
                          icons,
                          icons1,
                          icons2,
                          icons3,
                          activeItemhomePosition,
                          activeItemhomeElevation,
                          activeItemhomeMarginLeft,
                          activeItemhomeBottom,
                          activeItemhomeLeft,
                          onMenuItemPress,
                          onMenuItemPress1,
                          onMenuItemPress2,
                          onMenuItemPress3,
                        }) => {
  const activeItemhomeStyle = useMemo(() => {
    return {
      ...getStyleValue("position", activeItemhomePosition),
      ...getStyleValue("elevation", activeItemhomeElevation),
      ...getStyleValue("marginLeft", activeItemhomeMarginLeft),
      ...getStyleValue("bottom", activeItemhomeBottom),
      ...getStyleValue("left", activeItemhomeLeft),
    };
  }, [
    activeItemhomePosition,
    activeItemhomeElevation,
    activeItemhomeMarginLeft,
    activeItemhomeBottom,
    activeItemhomeLeft,
  ]);

  return (
      <View style={[styles.activeItemhome, activeItemhomeStyle]}>
        <Pressable onPress={onMenuItemPress}>
          <LinearGradient
              style={[styles.menuItem, styles.menuItemSpaceBlock]}
              locations={[0, 1]}
              colors={["#e7a5ec", "#dd7de1"]}
          >
            <Image style={styles.icons} contentFit="cover" source={icons} />
            <Text style={styles.home}>Home</Text>
          </LinearGradient>
        </Pressable>
        <Pressable onPress={onMenuItemPress1}>
          <View style={[styles.menuItem1, styles.menuItemSpaceBlock]}>
            <Image style={styles.icons} contentFit="cover" source={icons1} />
          </View>
        </Pressable>
        <Pressable onPress={onMenuItemPress2}>
          <View style={[styles.menuItem1, styles.menuItemSpaceBlock]}>
            <Image style={styles.icons2} contentFit="cover" source={icons2} />
          </View>
        </Pressable>
        <Pressable onPress={onMenuItemPress3}>
          <View style={[styles.menuItem1, styles.menuItemSpaceBlock]}>
            <Image style={styles.icons} contentFit="cover" source={icons3} />
          </View>
        </Pressable>
      </View>
  );
};

const styles = StyleSheet.create({
  menuItemSpaceBlock: {
    paddingVertical: Padding.p_7xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    borderRadius: Border.br_13xl,
    flexDirection: "row",
  },
  icons: {
    width: 24,
    height: 24,
    overflow: "hidden",
  },
  home: {
    fontSize: FontSize.headingH5_size,
    lineHeight: 11,
    fontWeight: "700",
    fontFamily: FontFamily.gBoardTiny,
    color: Color.neutralLightLightest,
    textAlign: "left",
    marginLeft: 5,
  },
  menuItem: {
    backgroundColor: "transparent",
  },
  menuItem1: {
    marginLeft: 19,
  },
  icons2: {
    width: 26,
    height: 25,
    overflow: "hidden",
  },
  activeItemhome: {
    borderRadius: Border.br_20xl,
    backgroundColor: Color.neutralLightLightest,
    shadowColor: "rgba(0, 0, 0, 0.25)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 10,
    elevation: 10,
    shadowOpacity: 1,
    paddingHorizontal: Padding.p_2xs,
    paddingVertical: Padding.p_10xs,
    flexDirection: "row",
  },
});

export default ActiveItemhome;
