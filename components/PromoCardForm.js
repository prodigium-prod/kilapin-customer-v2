import React, { useMemo } from "react";
import { StyleSheet, View, Text, ImageSourcePropType } from "react-native";
import { Image } from "expo-image";
import { Border, Color, FontSize, FontFamily } from "../GlobalStyles";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};
const PromoCardForm = ({
  rectangle63,
  bestDealWithF45TKILAPDisc,
  propLeft,
  propRight,
  propLeft1,
}) => {
  const promoStyle = useMemo(() => {
    return {
      ...getStyleValue("left", propLeft),
    };
  }, [propLeft]);

  const vectorIconStyle = useMemo(() => {
    return {
      ...getStyleValue("right", propRight),
      ...getStyleValue("left", propLeft1),
    };
  }, [propRight, propLeft1]);

  return (
    <View style={[styles.promo, styles.promoLayout, promoStyle]}>
      <View style={[styles.promoChild, styles.promoLayout]} />
      <Image style={styles.promoItem} contentFit="cover" source={rectangle63} />
      <Image
        style={[styles.vectorIcon, vectorIconStyle]}
        contentFit="cover"
        source={require("../assets/vector.png")}
      />
      <Text style={styles.bestDealWith}>{bestDealWithF45TKILAPDisc}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  promoLayout: {
    height: 170,
    width: 147,
    position: "absolute",
  },
  promoChild: {
    top: 0,
    left: 0,
    borderRadius: Border.br_xl,
    backgroundColor: Color.neutralLightLightest,
  },
  promoItem: {
    top: 6,
    left: 6,
    borderRadius: Border.br_mini,
    width: 134,
    height: 93,
    position: "absolute",
  },
  vectorIcon: {
    height: "14.12%",
    width: "16.33%",
    top: "310%",
    right: "31.29%",
    bottom: "-224.12%",
    left: "52.38%",
    maxWidth: "100%",
    overflow: "hidden",
    maxHeight: "100%",
    display: "none",
    position: "absolute",
  },
  bestDealWith: {
    top: 111,
    left: 14,
    fontSize: FontSize.bodyBodyM_size,
    lineHeight: 14,
    fontWeight: "600",
    fontFamily: FontFamily.gBoardTiny,
    color: Color.gBoard000000,
    textAlign: "left",
    width: 119,
    height: 39,
    position: "absolute",
  },
  promo: {
    top: 486,
    left: 16,
    shadowColor: "rgba(0, 0, 0, 0.03)",
    shadowOffset: {
      width: 0,
      height: 38,
    },
    shadowRadius: 145,
    elevation: 145,
    shadowOpacity: 1,
  },
});

export default PromoCardForm;
