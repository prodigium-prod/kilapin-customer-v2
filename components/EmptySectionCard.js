import * as React from "react";
import { StyleSheet, View, Text } from "react-native";
import { Color, Border, Padding, FontSize, FontFamily } from "../GlobalStyles";

const EmptySectionCard = () => {
  return (
    <View style={styles.imageParent}>
      <View style={[styles.image, styles.imageFlexBox]}>
        <View style={styles.image1}>
          <View style={[styles.fill, styles.fillPosition]} />
        </View>
      </View>
      <View style={styles.nothingHereForNowParent}>
        <Text style={[styles.nothingHereFor, styles.thisIsWhereLayout]}>
          Nothing here. For now.
        </Text>
        <Text
          style={[styles.thisIsWhere, styles.thisIsWhereLayout]}
        >{`This is where you’ll find your finished projects. `}</Text>
      </View>
      <View style={[styles.buttonPrimary, styles.imageFlexBox]}>
        <View style={styles.iconLayout}>
          <View style={[styles.fill1, styles.fillPosition]} />
        </View>
        <Text style={styles.button}>Book now</Text>
        <View style={[styles.rightIcon, styles.iconLayout]}>
          <View style={[styles.fill1, styles.fillPosition]} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  imageFlexBox: {
    justifyContent: "center",
    alignItems: "center",
  },
  fillPosition: {
    left: "0%",
    bottom: "0%",
    right: "0%",
    top: "0%",
    width: "100%",
    height: "100%",
    position: "absolute",
  },
  thisIsWhereLayout: {
    width: 238,
    textAlign: "center",
  },
  iconLayout: {
    display: "none",
    height: 12,
    width: 12,
  },
  fill: {
    backgroundColor: Color.colorPlum,
  },
  image1: {
    width: 32,
    height: 32,
    overflow: "hidden",
  },
  image: {
    borderRadius: Border.br_5xl,
    backgroundColor: Color.colorLavenderblush,
    width: 100,
    height: 100,
    padding: Padding.p_21xl,
  },
  nothingHereFor: {
    fontSize: FontSize.gBoardMedium_size,
    letterSpacing: 0.1,
    fontWeight: "900",
    fontFamily: FontFamily.headingH2,
    color: Color.neutralDarkDarkest,
  },
  thisIsWhere: {
    fontSize: FontSize.bodyBodyM_size,
    lineHeight: 20,
    fontFamily: FontFamily.bodyBodyS,
    color: Color.neutralDarkLight,
    marginTop: 8,
  },
  nothingHereForNowParent: {
    marginTop: 32,
  },
  fill1: {
    backgroundColor: Color.neutralLightLightest,
  },
  button: {
    fontSize: FontSize.headingH5_size,
    fontWeight: "600",
    fontFamily: FontFamily.actionActionM,
    color: Color.neutralLightLightest,
    textAlign: "left",
    marginLeft: 8,
  },
  rightIcon: {
    marginLeft: 8,
    overflow: "hidden",
  },
  buttonPrimary: {
    borderRadius: Border.br_xs,
    backgroundColor: Color.colorOrchid_100,
    height: 40,
    flexDirection: "row",
    paddingHorizontal: Padding.p_base,
    paddingVertical: Padding.p_xs,
    marginTop: 32,
    overflow: "hidden",
  },
  imageParent: {
    marginTop: -155,
    marginLeft: -180,
    top: "50%",
    left: "50%",
    width: 360,
    height: 310,
    padding: Padding.p_5xl,
    alignItems: "center",
    position: "absolute",
  },
});

export default EmptySectionCard;
