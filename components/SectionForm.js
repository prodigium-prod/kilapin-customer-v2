import * as React from "react";
import { StyleSheet, View, Pressable, Text } from "react-native";
import { Image } from "expo-image";
import { useNavigation } from "@react-navigation/native";
import { Color, Padding, FontSize, FontFamily, Border } from "../GlobalStyles";

const SectionForm = () => {
  const navigation = useNavigation();

  return (
    <View style={[styles.frameParent, styles.groupChildPosition]}>
      <View style={styles.messageInputWrapper}>
        <View style={[styles.messageInput, styles.sendFlexBox]}>
          <View style={styles.moreOptions}>
            <Pressable
              style={styles.add}
              onPress={() => navigation.navigate("ChatPage")}
            >
              <View style={[styles.fill, styles.fillPosition]} />
            </Pressable>
          </View>
          <Pressable
            style={[styles.textInput, styles.textFlexBox]}
            onPress={() => navigation.navigate("ChatPage1")}
          >
            <View style={styles.textFlexBox}>
              <Text style={styles.placeholder}>Type a message...</Text>
              <Image
                style={styles.cursorIcon}
                contentFit="cover"
                source={require("../assets/cursor.png")}
              />
            </View>
            <View style={[styles.send, styles.sendFlexBox]}>
              <View style={styles.icon}>
                <View style={[styles.fill1, styles.fillPosition]} />
              </View>
            </View>
          </Pressable>
        </View>
      </View>
      <View style={[styles.groupChild, styles.groupChildPosition]} />
    </View>
  );
};

const styles = StyleSheet.create({
  groupChildPosition: {
    width: 360,
    marginLeft: -180,
    left: "50%",
    position: "absolute",
  },
  sendFlexBox: {
    alignItems: "center",
    flexDirection: "row",
    overflow: "hidden",
  },
  fillPosition: {
    left: "0%",
    bottom: "0%",
    right: "0%",
    top: "0%",
    width: "100%",
    height: "100%",
    position: "absolute",
  },
  textFlexBox: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
  },
  fill: {
    backgroundColor: Color.colorOrchid_100,
  },
  add: {
    width: 16,
    height: 16,
    overflow: "hidden",
  },
  moreOptions: {
    padding: Padding.p_5xs,
    flexDirection: "row",
  },
  placeholder: {
    fontSize: FontSize.bodyBodyM_size,
    lineHeight: 20,
    fontFamily: FontFamily.bodyBodyS,
    color: Color.neutralDarkLightest,
    textAlign: "left",
  },
  cursorIcon: {
    width: 0,
    marginLeft: 1,
    display: "none",
    height: 16,
  },
  fill1: {
    backgroundColor: Color.neutralLightLightest,
  },
  icon: {
    width: 12,
    height: 12,
  },
  send: {
    borderRadius: Border.br_19xl,
    backgroundColor: Color.colorRoyalblue_100,
    width: 32,
    height: 32,
    justifyContent: "center",
    paddingHorizontal: Padding.p_16xl,
    paddingVertical: Padding.p_smi,
    marginLeft: 12,
    display: "none",
  },
  textInput: {
    borderRadius: Border.br_52xl,
    backgroundColor: Color.neutralLightLight,
    height: 40,
    paddingHorizontal: Padding.p_base,
    paddingVertical: Padding.p_5xs,
    marginLeft: 6,
    overflow: "hidden",
  },
  messageInput: {
    alignSelf: "stretch",
  },
  messageInputWrapper: {
    marginLeft: -179,
    bottom: 243,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    shadowOffset: {
      width: 0,
      height: -1,
    },
    shadowRadius: 10,
    elevation: 10,
    shadowOpacity: 1,
    width: 359,
    height: 68,
    padding: Padding.p_base,
    backgroundColor: Color.neutralLightLightest,
    left: "50%",
    position: "absolute",
  },
  groupChild: {
    bottom: 0,
    height: 243,
    backgroundColor: Color.neutralLightLightest,
  },
  frameParent: {
    bottom: 44,
    height: 311,
  },
});

export default SectionForm;
