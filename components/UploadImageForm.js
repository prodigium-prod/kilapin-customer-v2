import React, { useState, useCallback } from "react";
import { Text, StyleSheet, View, Pressable, Modal } from "react-native";
import SelectCountryRegion from "./SelectCountryRegion";
import { Color, FontFamily, FontSize } from "../GlobalStyles";

const UploadImageForm = () => {
  const [countryCodeContainerVisible, setCountryCodeContainerVisible] =
    useState(false);

  const openCountryCodeContainer = useCallback(() => {
    setCountryCodeContainerVisible(true);
  }, []);

  const closeCountryCodeContainer = useCallback(() => {
    setCountryCodeContainerVisible(false);
  }, []);

  return (
    <>
      <View style={styles.groupParent}>
        <View style={styles.oswinPrasetioParent}>
          <Text style={[styles.oswinPrasetio, styles.textTypo]}>
            Oswin Prasetio
          </Text>
          <View style={styles.groupChild} />
        </View>
        <View style={[styles.parent, styles.parentSpaceBlock]}>
          <Text style={[styles.text, styles.textTypo]}>817777771975</Text>
          <View style={[styles.groupItem, styles.groupItemPosition]} />
          <Pressable
            style={styles.countryCode}
            onPress={openCountryCodeContainer}
          >
            <Text style={[styles.text1, styles.textTypo]}>+62</Text>
            <View style={[styles.countryCodeChild, styles.groupItemPosition]} />
          </Pressable>
        </View>
        <View style={[styles.oswingmailcomParent, styles.parentSpaceBlock]}>
          <Text style={[styles.oswinPrasetio, styles.textTypo]}>
            oswin@gmail.com
          </Text>
          <View style={styles.groupChild} />
        </View>
      </View>

      <Modal
        animationType="fade"
        transparent
        visible={countryCodeContainerVisible}
      >
        <View style={styles.countryCodeContainerOverlay}>
          <Pressable
            style={styles.countryCodeContainerBg}
            onPress={closeCountryCodeContainer}
          />
          <SelectCountryRegion onClose={closeCountryCodeContainer} />
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  textTypo: {
    textAlign: "left",
    color: Color.gBoard000000,
    fontFamily: FontFamily.gBoardLarge,
    lineHeight: 11,
    fontSize: FontSize.headingH5_size,
    left: "50%",
    position: "absolute",
  },
  parentSpaceBlock: {
    marginTop: 19,
    width: 167,
  },
  groupItemPosition: {
    top: 21,
    height: 1,
    borderTopWidth: 1,
    borderColor: Color.colorDarkgray_100,
    borderStyle: "solid",
    position: "absolute",
  },
  oswinPrasetio: {
    marginLeft: -71.5,
    bottom: 9,
    color: Color.gBoard000000,
    fontFamily: FontFamily.gBoardLarge,
    lineHeight: 11,
    fontSize: FontSize.headingH5_size,
  },
  groupChild: {
    top: 20,
    width: 168,
    height: 1,
    borderTopWidth: 1,
    borderColor: Color.colorDarkgray_100,
    borderStyle: "solid",
    left: 0,
    position: "absolute",
  },
  oswinPrasetioParent: {
    height: 20,
    width: 167,
  },
  text: {
    marginLeft: -26.5,
    bottom: 9,
    color: Color.gBoard000000,
    fontFamily: FontFamily.gBoardLarge,
    lineHeight: 11,
    fontSize: FontSize.headingH5_size,
  },
  groupItem: {
    left: 45,
    width: 123,
  },
  countryCodeContainerOverlay: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(113, 113, 113, 0.3)",
  },
  countryCodeContainerBg: {
    position: "absolute",
    width: "100%",
    height: "100%",
    left: 0,
    top: 0,
  },
  text1: {
    marginLeft: -11,
    bottom: 10,
  },
  countryCodeChild: {
    width: 41,
    left: 0,
    top: 21,
  },
  countryCode: {
    marginLeft: -83.5,
    bottom: 0,
    width: 40,
    height: 21,
    left: "50%",
    position: "absolute",
  },
  parent: {
    height: 21,
  },
  oswingmailcomParent: {
    height: 20,
  },
  groupParent: {
    top: 182,
    left: 121,
    alignItems: "flex-end",
    position: "absolute",
  },
});

export default UploadImageForm;
