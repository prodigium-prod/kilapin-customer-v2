import React, { useMemo } from "react";
import { StyleSheet, View } from "react-native";
import { Image } from "expo-image";
import { Border, Color } from "../GlobalStyles";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};
const Property1Default = ({
  property1DefaultPosition,
  property1DefaultMarginLeft,
  property1DefaultTop,
  property1DefaultLeft,
  property1DefaultWidth,
}) => {
  const property1DefaultStyle = useMemo(() => {
    return {
      ...getStyleValue("position", property1DefaultPosition),
      ...getStyleValue("marginLeft", property1DefaultMarginLeft),
      ...getStyleValue("top", property1DefaultTop),
      ...getStyleValue("left", property1DefaultLeft),
      ...getStyleValue("width", property1DefaultWidth),
    };
  }, [
    property1DefaultPosition,
    property1DefaultMarginLeft,
    property1DefaultTop,
    property1DefaultLeft,
    property1DefaultWidth,
  ]);

  return (
    <View style={[styles.property1default, property1DefaultStyle]}>
      <View style={[styles.property1defaultChild, styles.iconLayout]} />
      <Image
        style={[styles.property1defaultItem, styles.property1defaultPosition]}
        contentFit="cover"
        source={require("../assets/frame-2.png")}
      />
      <View
        style={[styles.property1defaultInner, styles.property1defaultPosition]}
      >
        <View style={styles.parent}>
          <Image
            style={[styles.icon, styles.iconLayout]}
            contentFit="cover"
            source={require("../assets/3.png")}
          />
          <Image
            style={[styles.icon1, styles.iconLayout]}
            contentFit="cover"
            source={require("../assets/1.png")}
          />
          <Image
            style={[styles.icon1, styles.iconLayout]}
            contentFit="cover"
            source={require("../assets/2.png")}
          />
          <Image
            style={[styles.icon1, styles.iconLayout]}
            contentFit="cover"
            source={require("../assets/3.png")}
          />
          <Image
            style={[styles.icon1, styles.iconLayout]}
            contentFit="cover"
            source={require("../assets/1.png")}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  iconLayout: {
    width: 306,
    borderRadius: Border.br_sm,
    height: 221,
  },
  property1defaultPosition: {
    left: "50%",
    position: "absolute",
  },
  property1defaultChild: {
    marginLeft: -153,
    backgroundColor: Color.colorGainsboro_200,
    height: 221,
    left: "50%",
    position: "absolute",
    top: 9,
  },
  property1defaultItem: {
    marginLeft: -24,
    top: 243,
    width: 48,
    height: 8,
    overflow: "hidden",
  },
  icon: {
    height: 221,
  },
  icon1: {
    marginLeft: 11,
    height: 221,
  },
  parent: {
    top: 0,
    left: -290,
    flexDirection: "row",
    alignItems: "center",
    position: "absolute",
  },
  property1defaultInner: {
    marginLeft: -180,
    height: 221,
    top: 9,
    left: "50%",
    width: 360,
  },
  property1default: {
    height: 262,
    overflow: "hidden",
    width: 360,
  },
});

export default Property1Default;
