import * as React from "react";
import { StyleSheet, View, Pressable, Text } from "react-native";
import { Image } from "expo-image";
import { useNavigation } from "@react-navigation/native";
import { Color, Padding, FontSize, FontFamily, Border } from "../GlobalStyles";

const SectionCard = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.messageInputWrapper}>
      <View style={[styles.messageInput, styles.sendFlexBox]}>
        <Pressable
          style={styles.moreOptions}
          onPress={() => navigation.navigate("ChatPage")}
        >
          <View style={styles.add}>
            <View style={[styles.fill, styles.fillPosition]} />
          </View>
        </Pressable>
        <View style={[styles.textInput, styles.textFlexBox]}>
          <View style={styles.textFlexBox}>
            <Text style={styles.message} />
            <Image
              style={styles.cursorIcon}
              contentFit="cover"
              source={require("../assets/cursor1.png")}
            />
          </View>
          <View style={[styles.send, styles.sendFlexBox]}>
            <View style={styles.icon}>
              <View style={[styles.fill1, styles.fillPosition]} />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  sendFlexBox: {
    alignItems: "center",
    flexDirection: "row",
  },
  fillPosition: {
    left: "0%",
    bottom: "0%",
    right: "0%",
    top: "0%",
    width: "100%",
    height: "100%",
    position: "absolute",
  },
  textFlexBox: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
  },
  fill: {
    backgroundColor: Color.colorMediumorchid,
  },
  add: {
    width: 16,
    height: 16,
    overflow: "hidden",
  },
  moreOptions: {
    padding: Padding.p_5xs,
    flexDirection: "row",
  },
  message: {
    fontSize: FontSize.bodyBodyM_size,
    lineHeight: 20,
    fontFamily: FontFamily.bodyBodyS,
    color: Color.neutralDarkDarkest,
    textAlign: "left",
  },
  cursorIcon: {
    width: 2,
    height: 18,
    marginLeft: 1,
  },
  fill1: {
    backgroundColor: Color.neutralLightLightest,
  },
  icon: {
    width: 12,
    height: 12,
  },
  send: {
    borderRadius: Border.br_19xl,
    width: 32,
    height: 32,
    justifyContent: "center",
    paddingHorizontal: Padding.p_16xl,
    paddingVertical: Padding.p_smi,
    marginLeft: 12,
    backgroundColor: Color.colorMediumorchid,
    overflow: "hidden",
  },
  textInput: {
    borderRadius: Border.br_52xl,
    backgroundColor: Color.neutralLightLight,
    height: 40,
    paddingLeft: Padding.p_base,
    paddingTop: Padding.p_5xs,
    paddingRight: Padding.p_7xs,
    paddingBottom: Padding.p_5xs,
    marginLeft: 6,
    overflow: "hidden",
  },
  messageInput: {
    alignSelf: "stretch",
  },
  messageInputWrapper: {
    right: 1,
    bottom: 287,
    left: 0,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    shadowOffset: {
      width: 0,
      height: -1,
    },
    shadowRadius: 10,
    elevation: 10,
    shadowOpacity: 1,
    height: 68,
    padding: Padding.p_base,
    backgroundColor: Color.neutralLightLightest,
    position: "absolute",
  },
});

export default SectionCard;
