import * as React from "react";
import { View, StyleSheet, Pressable, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Image } from "expo-image";
import { useNavigation } from "@react-navigation/native";
import { Color, Border, FontFamily, FontSize } from "../GlobalStyles";

const Account = ({ onClose }) => {
  const navigation = useNavigation();

  return (
    <View style={[styles.account, styles.iconLayout1]}>
      <LinearGradient
        style={[styles.accountChild, styles.childLayout]}
        locations={[0, 1]}
        colors={["#f3f3f3", "#f8eef8"]}
      />
      <View style={[styles.rectangleParent, styles.childLayout]}>
        <LinearGradient
          style={[styles.frameChild, styles.childLayout]}
          locations={[0, 1]}
          colors={["#f3f3f3", "#f8eef8"]}
        />
        <LinearGradient
          style={styles.frameItem}
          locations={[0, 1]}
          colors={["#f3f3f3", "#f8eef8"]}
        />
      </View>
      <Pressable
        style={styles.vector}
        onPress={() => navigation.navigate("HOME")}
      >
        <Image
          style={[styles.icon, styles.iconLayout]}
          contentFit="cover"
          source={require("../assets/vector1.png")}
        />
      </Pressable>
      <View style={styles.profilePictureParent}>
        <Pressable
          style={styles.profilePicture}
          onPress={() => navigation.navigate("EditProfile")}
        >
          <Image
            style={styles.iconLayout}
            contentFit="cover"
            source={require("../assets/account.png")}
          />
        </Pressable>
        <View style={[styles.kilapeepsSince, styles.kilapeepsLayout]}>
          <View style={[styles.kilapeepsSinceChild, styles.childBg]} />
          <Text style={[styles.kilapeepsSince1, styles.totalOrder1Typo]}>
            Kilapeeps Since
          </Text>
          <Text style={[styles.august2023, styles.textClr]}>August 2023</Text>
        </View>
        <View style={[styles.totalOrder, styles.kilapeepsLayout]}>
          <View style={[styles.kilapeepsSinceChild, styles.childBg]} />
          <Text style={[styles.text, styles.textClr]}>100</Text>
          <Text style={[styles.totalOrder1, styles.totalOrder1Typo]}>
            Total Order
          </Text>
        </View>
        <Pressable
          style={[styles.membershipStatus, styles.membershipLayout]}
          onPress={() => navigation.navigate("Membership")}
        >
          <View
            style={[styles.membershipStatusChild, styles.membershipLayout]}
          />
          <Text style={[styles.membershipStatus1, styles.totalOrder1Typo]}>
            Membership Status
          </Text>
          <Image
            style={[styles.membershipStatusItem, styles.groupChildLayout1]}
            contentFit="cover"
            source={require("../assets/vector-11.png")}
          />
          <Text style={[styles.splendid, styles.xpTypo]}>Splendid</Text>
          <View
            style={[
              styles.membershipStatusInner,
              styles.membershipStatusInnerLayout,
            ]}
          />
          <LinearGradient
            style={[
              styles.rectangleLineargradient,
              styles.membershipStatusInnerLayout,
            ]}
            locations={[0, 1]}
            colors={["#214e4b", "#68c7c1"]}
          />
          <Text style={[styles.xp, styles.xpTypo]}>4.000/5.000 XP</Text>
        </Pressable>
        <View style={[styles.menu, styles.menuLayout]}>
          <View style={[styles.menuChild, styles.menuLayout]} />
          <View style={[styles.groupParent, styles.groupParentFlexBox]}>
            <View style={styles.parentLayout}>
              <Text style={[styles.helpCenterFaq, styles.textClr]}>
                Help Center (FaQ)
              </Text>
              <Image
                style={[styles.groupChild, styles.groupChildLayout1]}
                contentFit="cover"
                source={require("../assets/vector-12.png")}
              />
            </View>
            <View style={styles.frameInner} />
            <View style={[styles.languageParent, styles.parentLayout]}>
              <Text style={[styles.helpCenterFaq, styles.textClr]}>
                Language
              </Text>
              <Image
                style={[styles.groupChild, styles.groupChildLayout1]}
                contentFit="cover"
                source={require("../assets/vector-12.png")}
              />
            </View>
            <View style={styles.frameInner} />
            <View style={[styles.languageParent, styles.parentLayout]}>
              <Image
                style={[styles.groupChild, styles.groupChildLayout1]}
                contentFit="cover"
                source={require("../assets/vector-12.png")}
              />
              <Text
                style={[styles.helpCenterFaq, styles.textClr]}
              >{`Terms & Condition`}</Text>
            </View>
            <View style={styles.frameInner} />
            <View style={[styles.languageParent, styles.parentLayout]}>
              <Image
                style={[styles.groupChild, styles.groupChildLayout1]}
                contentFit="cover"
                source={require("../assets/vector-12.png")}
              />
              <Text style={[styles.helpCenterFaq, styles.textClr]}>
                Privacy Policy
              </Text>
            </View>
            <View style={styles.frameInner} />
            <View style={[styles.languageParent, styles.parentLayout]}>
              <Image
                style={[styles.groupChild, styles.groupChildLayout1]}
                contentFit="cover"
                source={require("../assets/vector-12.png")}
              />
              <Text style={[styles.helpCenterFaq, styles.textClr]}>
                My Voucher
              </Text>
            </View>
            <View style={styles.frameInner} />
            <View style={[styles.languageParent, styles.parentLayout]}>
              <Image
                style={[styles.groupChild, styles.groupChildLayout1]}
                contentFit="cover"
                source={require("../assets/vector-12.png")}
              />
              <Text style={[styles.helpCenterFaq, styles.textClr]}>
                Settings
              </Text>
            </View>
          </View>
        </View>
        <Image
          style={styles.kilapinMedal3x1Icon}
          contentFit="cover"
          source={require("../assets/kilapin-medal3x-1.png")}
        />
        <View style={[styles.displayName, styles.groupParentFlexBox]}>
          <Text style={[styles.name, styles.textClr]}>Oswin Prasetio</Text>
          <Image
            style={[styles.vectorIcon1, styles.vectorIcon1Layout]}
            contentFit="cover"
            source={require("../assets/vector2.png")}
          />
        </View>
      </View>
      <View style={styles.accountItem} />
      <View style={[styles.vectorParent1, styles.groupChild3Layout]}>
        <Image
          style={[styles.groupChild3, styles.groupChild3Layout]}
          contentFit="cover"
          source={require("../assets/vector-1.png")}
        />
        <View style={[styles.rectangleView, styles.vectorIcon1Layout]} />
        <View style={[styles.groupChild4, styles.groupChildLayout]} />
        <View style={[styles.groupChild5, styles.groupChildLayout]} />
        <View style={[styles.groupChild6, styles.groupChildLayout]} />
      </View>
      <View style={styles.accountInner} />
    </View>
  );
};

const styles = StyleSheet.create({
  iconLayout1: {
    maxHeight: "100%",
    maxWidth: "100%",
    overflow: "hidden",
  },
  childLayout: {
    height: 734,
    position: "absolute",
    width: 360,
  },
  iconLayout: {
    width: "100%",
    height: "100%",
  },
  kilapeepsLayout: {
    height: 48,
    width: 150,
    position: "absolute",
  },
  childBg: {
    backgroundColor: Color.neutralLightLightest,
    borderRadius: Border.br_mini,
    top: 0,
  },
  totalOrder1Typo: {
    color: Color.colorDimgray_100,
    fontFamily: FontFamily.gBoardLarge,
    lineHeight: 11,
    fontSize: FontSize.headingH5_size,
    textAlign: "left",
    position: "absolute",
  },
  textClr: {
    color: Color.gBoard000000,
    textAlign: "left",
  },
  membershipLayout: {
    height: 90,
    width: 305,
    left: "50%",
    position: "absolute",
  },
  groupChildLayout1: {
    height: 12,
    width: 7,
    position: "absolute",
  },
  xpTypo: {
    fontFamily: FontFamily.gBoardTiny,
    position: "absolute",
  },
  membershipStatusInnerLayout: {
    height: 28,
    borderRadius: Border.br_11xl,
    top: 49,
    position: "absolute",
  },
  menuLayout: {
    height: 238,
    position: "absolute",
  },
  groupParentFlexBox: {
    alignItems: "center",
    position: "absolute",
  },
  parentLayout: {
    height: 13,
    width: 261,
  },
  vectorIcon1Layout: {
    height: 14,
    width: 14,
  },
  groupChild3Layout: {
    height: 15,
    position: "absolute",
  },
  groupChildLayout: {
    width: 1,
    backgroundColor: Color.colorGray_300,
    height: 14,
    top: 0,
    position: "absolute",
  },
  accountChild: {
    backgroundColor: "transparent",
    shadowOpacity: 1,
    elevation: 34,
    shadowRadius: 34,
    shadowOffset: {
      width: 0,
      height: -20,
    },
    shadowColor: "rgba(0, 0, 0, 0.25)",
    bottom: 0,
    height: 734,
    borderTopRightRadius: Border.br_7xl,
    borderTopLeftRadius: Border.br_7xl,
    left: "50%",
    marginLeft: -180,
  },
  frameChild: {
    backgroundColor: "transparent",
    shadowOpacity: 1,
    elevation: 34,
    shadowRadius: 34,
    shadowOffset: {
      width: 0,
      height: -20,
    },
    shadowColor: "rgba(0, 0, 0, 0.25)",
    bottom: 0,
    height: 734,
    borderTopRightRadius: Border.br_7xl,
    borderTopLeftRadius: Border.br_7xl,
    left: "50%",
    marginLeft: -180,
  },
  frameItem: {
    bottom: 671,
    height: 63,
    backgroundColor: "transparent",
    borderTopRightRadius: Border.br_7xl,
    borderTopLeftRadius: Border.br_7xl,
    left: "50%",
    marginLeft: -180,
    position: "absolute",
    width: 360,
  },
  rectangleParent: {
    top: 66,
    left: 0,
  },
  icon: {
    maxHeight: "100%",
    maxWidth: "100%",
    overflow: "hidden",
  },
  vector: {
    left: "7.5%",
    top: "11.38%",
    right: "88.06%",
    bottom: "86.63%",
    width: "4.44%",
    height: "2%",
    position: "absolute",
  },
  profilePicture: {
    left: 10,
    width: 96,
    height: 96,
    top: 0,
    position: "absolute",
  },
  kilapeepsSinceChild: {
    height: 48,
    width: 150,
    position: "absolute",
    left: 0,
  },
  kilapeepsSince1: {
    top: 9,
    textAlign: "left",
    left: 15,
  },
  august2023: {
    top: 25,
    fontFamily: FontFamily.gBoardTiny,
    position: "absolute",
    fontWeight: "700",
    lineHeight: 13,
    fontSize: FontSize.bodyBodyM_size,
    left: 15,
  },
  kilapeepsSince: {
    elevation: 10,
    shadowRadius: 10,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -20,
    },
    left: 1,
    width: 150,
    top: 144,
  },
  text: {
    top: 26,
    fontFamily: FontFamily.gBoardTiny,
    position: "absolute",
    fontWeight: "700",
    lineHeight: 13,
    fontSize: FontSize.bodyBodyM_size,
    left: 15,
  },
  totalOrder1: {
    top: 10,
    textAlign: "left",
    left: 15,
  },
  totalOrder: {
    left: 156,
    elevation: 10,
    shadowRadius: 10,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -20,
    },
    top: 144,
    width: 150,
  },
  membershipStatusChild: {
    marginLeft: -152.5,
    backgroundColor: Color.neutralLightLightest,
    borderRadius: Border.br_mini,
    top: 0,
  },
  membershipStatus1: {
    top: 12,
    left: 13,
    textAlign: "left",
  },
  membershipStatusItem: {
    top: 13,
    left: 279,
  },
  splendid: {
    top: 28,
    left: 14,
    color: "#214e4b",
    fontWeight: "700",
    fontFamily: FontFamily.gBoardTiny,
    lineHeight: 13,
    fontSize: FontSize.bodyBodyM_size,
    textAlign: "left",
  },
  membershipStatusInner: {
    marginLeft: -139.5,
    backgroundColor: Color.colorGainsboro_100,
    width: 280,
    left: "50%",
  },
  rectangleLineargradient: {
    width: 225,
    left: 13,
    backgroundColor: "transparent",
  },
  xp: {
    top: 59,
    left: 90,
    fontSize: FontSize.bodyBodyXS_size,
    lineHeight: 9,
    fontWeight: "600",
    color: Color.neutralLightLightest,
    textAlign: "left",
  },
  membershipStatus: {
    marginLeft: -152,
    top: 207,
    elevation: 10,
    shadowRadius: 10,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -20,
    },
  },
  menuChild: {
    width: 305,
    height: 238,
    backgroundColor: Color.neutralLightLightest,
    borderRadius: Border.br_mini,
    left: 1,
    top: 0,
  },
  helpCenterFaq: {
    lineHeight: 13,
    fontSize: FontSize.bodyBodyM_size,
    color: Color.gBoard000000,
    fontFamily: FontFamily.gBoardLarge,
    top: 0,
    left: 0,
    position: "absolute",
  },
  groupChild: {
    top: 1,
    left: 255,
  },
  frameInner: {
    borderColor: Color.colorWhitesmoke_300,
    borderTopWidth: 1,
    width: 307,
    height: 1,
    marginTop: 13,
    borderStyle: "solid",
  },
  languageParent: {
    marginTop: 13,
  },
  groupParent: {
    top: 15,
    left: 0,
  },
  menu: {
    top: 312,
    elevation: 10,
    shadowRadius: 10,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -20,
    },
    width: 306,
    left: 0,
  },
  kilapinMedal3x1Icon: {
    top: 45,
    left: 58,
    width: 67,
    height: 67,
    position: "absolute",
  },
  name: {
    fontSize: FontSize.size_xl,
    lineHeight: 18,
    fontWeight: "900",
    fontFamily: FontFamily.robotoBlack,
  },
  vectorIcon1: {
    marginLeft: 7,
  },
  displayName: {
    top: 111,
    left: 9,
    flexDirection: "row",
  },
  profilePictureParent: {
    marginLeft: -154,
    top: 130,
    height: 550,
    width: 306,
    left: "50%",
    position: "absolute",
  },
  accountItem: {
    top: 755,
    backgroundColor: Color.colorWhitesmoke_200,
    height: 45,
    left: 0,
    position: "absolute",
    width: 360,
  },
  groupChild3: {
    left: 203,
    width: 9,
    top: 0,
  },
  rectangleView: {
    left: 100,
    borderRadius: Border.br_9xs,
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    borderStyle: "solid",
    top: 0,
    position: "absolute",
  },
  groupChild4: {
    left: 0,
  },
  groupChild5: {
    left: 6,
  },
  groupChild6: {
    left: 12,
  },
  vectorParent1: {
    top: 771,
    left: 74,
    width: 212,
  },
  accountInner: {
    bottom: -14,
    height: 699,
    left: "50%",
    marginLeft: -180,
    position: "absolute",
    width: 360,
  },
  account: {
    height: 800,
    width: 360,
  },
});

export default Account;
