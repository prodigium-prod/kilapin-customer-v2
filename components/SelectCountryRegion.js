import * as React from "react";
import { View, StyleSheet, Text } from "react-native";
import { Image } from "expo-image";
import { LinearGradient } from "expo-linear-gradient";
import { Border, Color, FontSize, FontFamily } from "../GlobalStyles";

const SelectCountryRegion = ({ onClose }) => {
  return (
    <View style={[styles.selectCountryregion, styles.selectPosition1]}>
      <View style={styles.selectCountryregionChild} />
      <View style={[styles.selectCountryregionItem, styles.selectPosition]} />
      <View style={styles.vectorParent}>
        <Image
          style={styles.groupChild}
          contentFit="cover"
          source={require("../assets/vector-1.png")}
        />
        <View style={styles.groupItem} />
        <View style={[styles.groupInner, styles.groupLayout]} />
        <View style={[styles.rectangleView, styles.groupLayout]} />
        <View style={[styles.groupChild1, styles.groupLayout]} />
      </View>
      <View style={[styles.selectCountryregionInner, styles.selectBg]} />
      <View style={[styles.selectCountryregionChild1, styles.selectPosition]} />
      <LinearGradient
        style={styles.rectangleLineargradient}
        locations={[0, 1]}
        colors={["#f3f3f3", "#f8eef8"]}
      />
      <Text style={styles.selectCountry}>Select Country / Region</Text>
      <View
        style={[styles.selectCountryregionChild2, styles.selectChildLayout]}
      />
      <View
        style={[styles.selectCountryregionChild3, styles.selectChildLayout]}
      />
      <View style={[styles.selectCountryregionChild4, styles.selectBg]} />
      <Text style={[styles.search, styles.searchTypo]}>Search</Text>
      <Text style={[styles.cancel, styles.searchTypo]}>Cancel</Text>
      <View style={styles.lineView} />
    </View>
  );
};

const styles = StyleSheet.create({
  selectPosition1: {
    borderTopRightRadius: Border.br_7xl,
    borderTopLeftRadius: Border.br_7xl,
  },
  selectPosition: {
    backgroundColor: Color.colorWhitesmoke_300,
    left: 0,
    position: "absolute",
    width: 360,
  },
  groupLayout: {
    width: 1,
    backgroundColor: Color.colorGray_300,
    top: 0,
    height: 14,
    position: "absolute",
  },
  selectBg: {
    backgroundColor: Color.colorGainsboro_200,
    position: "absolute",
  },
  selectChildLayout: {
    height: 877,
    width: 16,
    borderColor: Color.colorLime,
    top: -77,
    borderWidth: 1,
    borderStyle: "solid",
    position: "absolute",
  },
  searchTypo: {
    lineHeight: 13,
    fontSize: FontSize.bodyBodyM_size,
    textAlign: "center",
    position: "absolute",
  },
  selectCountryregionChild: {
    top: 755,
    backgroundColor: Color.colorWhitesmoke_200,
    height: 45,
    left: 0,
    position: "absolute",
    width: 360,
  },
  selectCountryregionItem: {
    top: 135,
    height: 881,
  },
  groupChild: {
    top: 1,
    left: 204,
    width: 8,
    height: 14,
    position: "absolute",
  },
  groupItem: {
    left: 100,
    borderRadius: Border.br_9xs,
    borderColor: Color.colorGray_200,
    width: 14,
    borderWidth: 1,
    top: 0,
    borderStyle: "solid",
    height: 14,
    position: "absolute",
  },
  groupInner: {
    left: 0,
  },
  rectangleView: {
    left: 6,
  },
  groupChild1: {
    left: 12,
  },
  vectorParent: {
    top: 771,
    left: 74,
    width: 212,
    height: 15,
    position: "absolute",
  },
  selectCountryregionInner: {
    top: 703,
    height: 52,
    left: 0,
    width: 360,
  },
  selectCountryregionChild1: {
    top: 40,
    height: 97,
    borderTopRightRadius: Border.br_7xl,
    borderTopLeftRadius: Border.br_7xl,
  },
  rectangleLineargradient: {
    marginLeft: -180,
    bottom: 0,
    shadowColor: "rgba(0, 0, 0, 0.25)",
    shadowOffset: {
      width: 0,
      height: -20,
    },
    shadowRadius: 34,
    elevation: 34,
    shadowOpacity: 1,
    height: 615,
    display: "none",
    backgroundColor: "transparent",
    left: "50%",
    position: "absolute",
    width: 360,
    borderTopRightRadius: Border.br_7xl,
    borderTopLeftRadius: Border.br_7xl,
  },
  selectCountry: {
    marginLeft: -86,
    top: 62,
    fontSize: FontSize.size_base,
    lineHeight: 14,
    fontWeight: "700",
    fontFamily: FontFamily.gBoardTiny,
    color: Color.gBoard000000,
    textAlign: "center",
    left: "50%",
    position: "absolute",
  },
  selectCountryregionChild2: {
    left: 344,
  },
  selectCountryregionChild3: {
    left: 0,
  },
  selectCountryregionChild4: {
    top: 93,
    left: 26,
    borderRadius: 5,
    width: 257,
    height: 30,
  },
  search: {
    top: 102,
    left: 51,
    fontFamily: FontFamily.gBoardLarge,
    color: "#626064",
  },
  cancel: {
    top: 101,
    left: 291,
    fontWeight: "500",
    fontFamily: FontFamily.gBoardDefault,
    color: Color.colorOrchid_100,
  },
  lineView: {
    top: 137,
    borderColor: Color.colorGainsboro_200,
    borderTopWidth: 1,
    width: 361,
    height: 1,
    borderStyle: "solid",
    left: 0,
    position: "absolute",
  },
  selectCountryregion: {
    height: 655,
    overflow: "hidden",
    maxWidth: "100%",
    maxHeight: "100%",
    width: 360,
  },
});

export default SelectCountryRegion;
