import * as React from "react";
import { Image } from "expo-image";
import { StyleSheet, View, Pressable, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { useNavigation } from "@react-navigation/native";
import { Padding, Border, Color, FontSize, FontFamily } from "../GlobalStyles";

const ActivitySection = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.menuParent}>
      <View style={styles.menu}>
        <Pressable
          style={styles.menuItem}
          onPress={() => navigation.navigate("HOME")}
        >
          <Image
            style={styles.icons}
            contentFit="cover"
            source={require("../assets/icons.png")}
          />
        </Pressable>
        <Pressable
          style={styles.menuItemSpaceBlock}
          onPress={() => navigation.navigate("NewsPromo")}
        >
          <Image
            style={styles.icons}
            contentFit="cover"
            source={require("../assets/icons1.png")}
          />
        </Pressable>
        <LinearGradient
          style={[styles.menuItem2, styles.menuItemSpaceBlock]}
          locations={[0, 1]}
          colors={["#e7a5ec", "#dd7de1"]}
        >
          <Image
            style={styles.icons2}
            contentFit="cover"
            source={require("../assets/icons6.png")}
          />
          <Text style={styles.activity}>Activity</Text>
        </LinearGradient>
        <Pressable
          style={styles.menuItemSpaceBlock}
          onPress={() => navigation.navigate("Chat")}
        >
          <Image
            style={styles.icons}
            contentFit="cover"
            source={require("../assets/icons5.png")}
          />
        </Pressable>
      </View>
      <View style={styles.homePosition}>
        <View style={[styles.homeBackButtonChild, styles.homePosition]} />
        <View style={[styles.vectorParent, styles.groupChildPosition]}>
          <Image
            style={[styles.groupChild, styles.groupChildPosition]}
            contentFit="cover"
            source={require("../assets/vector-1.png")}
          />
          <View style={styles.groupItem} />
          <View style={[styles.groupInner, styles.groupPosition]} />
          <View style={[styles.rectangleView, styles.groupPosition]} />
          <View style={[styles.groupChild1, styles.groupPosition]} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  menuItemSpaceBlock: {
    marginLeft: 19,
    paddingVertical: Padding.p_7xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    borderRadius: Border.br_13xl,
    flexDirection: "row",
  },
  homePosition: {
    height: 45,
    width: 360,
    left: "50%",
    bottom: 0,
    marginLeft: -180,
    position: "absolute",
  },
  groupChildPosition: {
    height: 15,
    left: "50%",
    position: "absolute",
  },
  groupPosition: {
    width: 1,
    backgroundColor: Color.colorGray_300,
    height: 14,
    bottom: 1,
    left: "50%",
    position: "absolute",
  },
  icons: {
    width: 24,
    height: 24,
    overflow: "hidden",
  },
  menuItem: {
    paddingVertical: Padding.p_7xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    borderRadius: Border.br_13xl,
    flexDirection: "row",
  },
  icons2: {
    width: 26,
    height: 25,
    overflow: "hidden",
  },
  activity: {
    fontSize: FontSize.headingH5_size,
    lineHeight: 11,
    fontWeight: "700",
    fontFamily: FontFamily.gBoardTiny,
    color: Color.neutralLightLightest,
    textAlign: "left",
    marginLeft: 5,
  },
  menuItem2: {
    backgroundColor: "transparent",
  },
  menu: {
    marginLeft: -159.5,
    bottom: 71,
    borderRadius: Border.br_20xl,
    backgroundColor: Color.neutralLightLightest,
    shadowColor: "rgba(221, 125, 225, 0.25)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 15,
    elevation: 15,
    shadowOpacity: 1,
    paddingHorizontal: Padding.p_2xs,
    paddingVertical: Padding.p_5xs,
    flexDirection: "row",
    left: "50%",
    position: "absolute",
  },
  homeBackButtonChild: {
    backgroundColor: Color.colorWhitesmoke_200,
  },
  groupChild: {
    marginLeft: 97.52,
    width: 9,
    bottom: 0,
    height: 15,
  },
  groupItem: {
    marginLeft: -6.25,
    borderRadius: Border.br_9xs,
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    width: 14,
    height: 14,
    bottom: 1,
    left: "50%",
    position: "absolute",
  },
  groupInner: {
    marginLeft: -105.75,
  },
  rectangleView: {
    marginLeft: -99.75,
  },
  groupChild1: {
    marginLeft: -93.75,
  },
  vectorParent: {
    marginLeft: -106,
    bottom: 15,
    width: 212,
  },
  menuParent: {
    height: 124,
    width: 360,
    left: "50%",
    marginLeft: -180,
    position: "absolute",
    bottom: 0,
  },
});

export default ActivitySection;
