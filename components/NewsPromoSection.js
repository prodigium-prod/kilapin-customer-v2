import * as React from "react";
import { StyleSheet, View, Pressable, Text } from "react-native";
import { Image } from "expo-image";
import { LinearGradient } from "expo-linear-gradient";
import { useNavigation } from "@react-navigation/native";
import { Color, Padding, Border, FontSize, FontFamily } from "../GlobalStyles";

const NewsPromoSection = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.groupParent}>
      <View style={styles.groupChildPosition1}>
        <View style={[styles.groupChild, styles.groupChildPosition1]} />
        <View style={[styles.vectorParent, styles.groupItemPosition]}>
          <Image
            style={[styles.groupItem, styles.groupItemPosition]}
            contentFit="cover"
            source={require("../assets/vector-1.png")}
          />
          <View style={styles.groupInner} />
          <View style={[styles.rectangleView, styles.groupChildPosition]} />
          <View style={[styles.groupChild1, styles.groupChildPosition]} />
          <View style={[styles.groupChild2, styles.groupChildPosition]} />
        </View>
      </View>
      <View style={styles.menu}>
        <Pressable
          style={styles.menuItem}
          onPress={() => navigation.navigate("HOME")}
        >
          <Image
            style={styles.icons}
            contentFit="cover"
            source={require("../assets/icons.png")}
          />
        </Pressable>
        <LinearGradient
          style={[styles.menuItem1, styles.menuItemSpaceBlock]}
          locations={[0, 1]}
          colors={["#e7a5ec", "#dd7de1"]}
        >
          <Image
            style={styles.icons}
            contentFit="cover"
            source={require("../assets/icons4.png")}
          />
          <Text style={styles.newsPromo}>{`News &`}Promo</Text>
        </LinearGradient>
        <Pressable
          style={styles.menuItemSpaceBlock}
          onPress={() => navigation.navigate("ActivityHistory")}
        >
          <Image
            style={styles.icons2}
            contentFit="cover"
            source={require("../assets/icons2.png")}
          />
        </Pressable>
        <Pressable
          style={styles.menuItemSpaceBlock}
          onPress={() => navigation.navigate("Chat")}
        >
          <Image
            style={styles.icons}
            contentFit="cover"
            source={require("../assets/icons5.png")}
          />
        </Pressable>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  groupChildPosition1: {
    height: 45,
    width: 360,
    left: "50%",
    bottom: 0,
    marginLeft: -180,
    position: "absolute",
  },
  groupItemPosition: {
    height: 15,
    left: "50%",
    position: "absolute",
  },
  groupChildPosition: {
    width: 1,
    backgroundColor: Color.colorGray_300,
    height: 14,
    bottom: 1,
    left: "50%",
    position: "absolute",
  },
  menuItemSpaceBlock: {
    marginLeft: 19,
    paddingVertical: Padding.p_7xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    borderRadius: Border.br_13xl,
    flexDirection: "row",
  },
  groupChild: {
    backgroundColor: Color.colorWhitesmoke_200,
  },
  groupItem: {
    marginLeft: 97.52,
    width: 9,
    bottom: 0,
    height: 15,
  },
  groupInner: {
    marginLeft: -6.25,
    borderRadius: Border.br_9xs,
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    width: 14,
    height: 14,
    bottom: 1,
    left: "50%",
    position: "absolute",
  },
  rectangleView: {
    marginLeft: -105.75,
  },
  groupChild1: {
    marginLeft: -99.75,
  },
  groupChild2: {
    marginLeft: -93.75,
  },
  vectorParent: {
    marginLeft: -106,
    bottom: 15,
    width: 212,
  },
  icons: {
    width: 24,
    height: 24,
    overflow: "hidden",
  },
  menuItem: {
    paddingVertical: Padding.p_7xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    borderRadius: Border.br_13xl,
    flexDirection: "row",
  },
  newsPromo: {
    fontSize: FontSize.headingH5_size,
    lineHeight: 11,
    fontWeight: "700",
    fontFamily: FontFamily.gBoardTiny,
    color: Color.neutralLightLightest,
    textAlign: "left",
    marginLeft: 5,
  },
  menuItem1: {
    backgroundColor: "transparent",
  },
  icons2: {
    width: 26,
    height: 25,
    overflow: "hidden",
  },
  menu: {
    marginLeft: -159.5,
    bottom: 71,
    borderRadius: Border.br_20xl,
    backgroundColor: Color.neutralLightLightest,
    shadowColor: "rgba(221, 125, 225, 0.26)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 15,
    elevation: 15,
    shadowOpacity: 1,
    paddingHorizontal: Padding.p_2xs,
    paddingVertical: Padding.p_5xs,
    flexDirection: "row",
    left: "50%",
    position: "absolute",
  },
  groupParent: {
    height: 124,
    width: 360,
    left: "50%",
    marginLeft: -180,
    position: "absolute",
    bottom: 0,
  },
});

export default NewsPromoSection;
