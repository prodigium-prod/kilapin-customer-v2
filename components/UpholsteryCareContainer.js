import * as React from "react";
import { Text, StyleSheet, View, ImageSourcePropType } from "react-native";
import { Image } from "expo-image";
import { LinearGradient } from "expo-linear-gradient";
import { Color, Border, FontSize, FontFamily } from "../GlobalStyles";

const UpholsteryCareContainer = ({
  specializingInFabricAndLe,
  upholsteryCare,
  rectangle17,
}) => {
  return (
    <View style={[styles.frameParent, styles.frameLayout1]}>
      <View style={styles.specializingInFabricAndLeaParent}>
        <Text
          style={[styles.specializingInFabric, styles.upholsteryCareFlexBox]}
        >
          {specializingInFabricAndLe}
        </Text>
        <Text style={[styles.upholsteryCare, styles.framePosition]}>
          {upholsteryCare}
        </Text>
      </View>
      <View style={[styles.frame, styles.framePosition]}>
        <Image
          style={styles.frameLayout}
          contentFit="cover"
          source={rectangle17}
        />
        <LinearGradient
          style={[styles.frameItem, styles.frameLayout]}
          locations={[0, 1]}
          colors={["rgba(221, 125, 225, 0.39)", "#dd7de1"]}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  frameLayout1: {
    height: 214,
    width: 146,
  },
  upholsteryCareFlexBox: {
    textAlign: "left",
    color: Color.neutralLightLightest,
    width: 112,
  },
  framePosition: {
    top: 0,
    left: 0,
    position: "absolute",
  },
  frameLayout: {
    borderRadius: Border.br_sm,
    top: 0,
    left: 0,
    position: "absolute",
    height: 214,
    width: 146,
  },
  specializingInFabric: {
    top: 41,
    fontSize: FontSize.bodyBodyXS_size,
    fontFamily: FontFamily.bodyBodyS,
    height: 24,
    left: 0,
    color: Color.neutralLightLightest,
    position: "absolute",
  },
  upholsteryCare: {
    fontSize: FontSize.size_base,
    fontWeight: "700",
    fontFamily: FontFamily.headingH5,
    height: 33,
    textAlign: "left",
    color: Color.neutralLightLightest,
    width: 112,
  },
  specializingInFabricAndLeaParent: {
    top: 129,
    left: 17,
    height: 65,
    width: 112,
    position: "absolute",
  },
  frameItem: {
    backgroundColor: "transparent",
  },
  frame: {
    overflow: "hidden",
    height: 214,
    width: 146,
  },
  frameParent: {
    shadowColor: "rgba(221, 125, 225, 0.5)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8,
    elevation: 8,
    shadowOpacity: 1,
    marginLeft: 12,
  },
});

export default UpholsteryCareContainer;
