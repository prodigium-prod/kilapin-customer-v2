import React, { useMemo } from "react";
import { StyleSheet, View, Text } from "react-native";
import { Image } from "expo-image";
import { Border, Color, FontSize, FontFamily } from "../GlobalStyles";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};
const NewsCard = ({ propLeft }) => {
  const newsStyle = useMemo(() => {
    return {
      ...getStyleValue("left", propLeft),
    };
  }, [propLeft]);

  return (
    <View style={[styles.news, styles.newsLayout, newsStyle]}>
      <View style={[styles.newsChild, styles.newsLayout]} />
      <Image
        style={styles.imageIcon}
        contentFit="cover"
        source={require("../assets/image.png")}
      />
      <Text
        style={[styles.title, styles.datePosition]}
      >{`Tips to Make Your Home More Comfortable
`}</Text>
      <Text style={[styles.date, styles.datePosition]}>16 Aug 2023</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  newsLayout: {
    height: 283,
    width: 311,
    position: "absolute",
  },
  datePosition: {
    textAlign: "left",
    left: 11,
    position: "absolute",
  },
  newsChild: {
    top: 0,
    left: 0,
    borderRadius: Border.br_base,
    backgroundColor: Color.neutralLightLightest,
  },
  imageIcon: {
    top: 10,
    borderRadius: Border.br_3xs,
    height: 200,
    width: 289,
    left: 11,
    position: "absolute",
  },
  title: {
    top: 220,
    fontSize: FontSize.size_base,
    lineHeight: 16,
    fontWeight: "700",
    fontFamily: FontFamily.gBoardTiny,
    color: Color.gBoard000000,
    height: 27,
    width: 289,
  },
  date: {
    top: 257,
    fontSize: FontSize.headingH5_size,
    lineHeight: 12,
    fontFamily: FontFamily.gBoardLarge,
    color: Color.colorGray_200,
    width: 74,
  },
  news: {
    top: 154,
    left: 16,
    shadowColor: "rgba(0, 0, 0, 0.03)",
    shadowOffset: {
      width: 0,
      height: 38,
    },
    shadowRadius: 145,
    elevation: 145,
    shadowOpacity: 1,
  },
});

export default NewsCard;
