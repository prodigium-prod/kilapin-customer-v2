import React, { useState, useCallback } from "react";
import { Text, StyleSheet, View, Pressable, Modal } from "react-native";
import { Image } from "expo-image";
import Account from "./Account";
import { Color, FontFamily, FontSize } from "../GlobalStyles";

const WelcomeHomeCard = () => {
  const [accountIconVisible, setAccountIconVisible] = useState(false);

  const openAccountIcon = useCallback(() => {
    setAccountIconVisible(true);
  }, []);

  const closeAccountIcon = useCallback(() => {
    setAccountIconVisible(false);
  }, []);

  return (
    <>
      <View style={[styles.frame, styles.framePosition2]}>
        <View style={[styles.frame1, styles.framePosition1]}>
          <View style={[styles.frame2, styles.framePosition1]}>
            <Text
              style={[styles.welcomeHomeKilapeeps, styles.allRightLetsPosition]}
            >
              Welcome home, Kilapeeps
            </Text>
            <Text style={[styles.allRightLets, styles.allRightLetsPosition]}>
              All right, let’s get a clean house today.
            </Text>
          </View>
        </View>
        <View style={[styles.frame3, styles.framePosition]}>
          <Pressable
            style={[styles.account, styles.accountPosition]}
            onPress={openAccountIcon}
          >
            <Image
              style={[styles.icon, styles.iconLayout]}
              contentFit="cover"
              source={require("../assets/account1.png")}
            />
          </Pressable>
          <View style={[styles.frame4, styles.framePosition]}>
            <Text style={[styles.location, styles.locationTypo]}>Location</Text>
            <View style={[styles.frame5, styles.framePosition2]}>
              <Text style={[styles.kelapaGadingJakarta, styles.locationTypo]}>
                Kelapa Gading, Jakarta
              </Text>
              <Image
                style={[styles.frameChild, styles.iconLayout]}
                contentFit="cover"
                source={require("../assets/group-87.png")}
              />
            </View>
          </View>
        </View>
      </View>

      <Modal animationType="fade" transparent visible={accountIconVisible}>
        <View style={styles.accountIconOverlay}>
          <Pressable style={styles.accountIconBg} onPress={closeAccountIcon} />
          <Account onClose={closeAccountIcon} />
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  framePosition2: {
    overflow: "hidden",
    position: "absolute",
  },
  framePosition1: {
    height: 40,
    left: 0,
    overflow: "hidden",
    position: "absolute",
  },
  allRightLetsPosition: {
    textAlign: "left",
    color: Color.gBoard000000,
    left: 0,
    position: "absolute",
  },
  framePosition: {
    height: 37,
    left: 0,
    overflow: "hidden",
    top: 0,
    position: "absolute",
  },
  accountPosition: {
    top: "0%",
    position: "absolute",
  },
  iconLayout: {
    maxHeight: "100%",
    maxWidth: "100%",
    height: "100%",
    overflow: "hidden",
  },
  locationTypo: {
    fontFamily: FontFamily.interMedium,
    fontWeight: "500",
    fontSize: FontSize.headingH5_size,
    textAlign: "left",
    position: "absolute",
  },
  welcomeHomeKilapeeps: {
    fontSize: FontSize.size_xl,
    fontWeight: "700",
    fontFamily: FontFamily.headingH5,
    top: 0,
  },
  allRightLets: {
    top: 23,
    fontSize: FontSize.bodyBodyM_size,
    fontFamily: FontFamily.bodyBodyS,
  },
  frame2: {
    width: 259,
    top: 0,
  },
  frame1: {
    top: 51,
    width: 314,
  },
  accountIconOverlay: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(113, 113, 113, 0.3)",
  },
  accountIconBg: {
    position: "absolute",
    width: "100%",
    height: "100%",
    left: 0,
    top: 0,
  },
  icon: {
    width: "100%",
  },
  account: {
    left: "89.17%",
    right: "0%",
    bottom: "8.11%",
    width: "10.83%",
    height: "91.89%",
  },
  location: {
    color: Color.colorGray_200,
    fontFamily: FontFamily.interMedium,
    fontWeight: "500",
    fontSize: FontSize.headingH5_size,
    left: 0,
    top: 0,
  },
  kelapaGadingJakarta: {
    top: 1,
    left: 20,
    width: 200,
    color: Color.gBoard000000,
    fontFamily: FontFamily.interMedium,
    fontWeight: "500",
    fontSize: FontSize.headingH5_size,
  },
  frameChild: {
    width: "6.02%",
    right: "93.98%",
    bottom: "0%",
    left: "0%",
    top: "0%",
    position: "absolute",
  },
  frame5: {
    top: 20,
    left: 3,
    width: 220,
    height: 17,
  },
  frame4: {
    width: 223,
  },
  frame3: {
    width: 314,
  },
  frame: {
    left: 23,
    height: 91,
    width: 314,
    top: 0,
  },
});

export default WelcomeHomeCard;
