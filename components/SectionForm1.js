import * as React from "react";
import { StyleSheet, View, Pressable, Text } from "react-native";
import { Image } from "expo-image";
import { useNavigation } from "@react-navigation/native";
import { Color, Padding, FontSize, FontFamily, Border } from "../GlobalStyles";

const SectionForm1 = () => {
  const navigation = useNavigation();

  return (
    <View style={[styles.frameParent, styles.frameParentPosition]}>
      <View style={[styles.messageInputWrapper, styles.frameParentPosition]}>
        <View style={[styles.messageInput, styles.sendFlexBox]}>
          <View style={styles.moreOptions}>
            <Pressable
              style={styles.add}
              onPress={() => navigation.navigate("ChatPage")}
            >
              <View style={[styles.fill, styles.fillPosition]} />
            </Pressable>
          </View>
          <Pressable
            style={[styles.textInput, styles.textFlexBox]}
            onPress={() => navigation.navigate("ChatPage1")}
          >
            <View style={styles.textFlexBox}>
              <Text style={styles.placeholder}>Type a message...</Text>
              <Image
                style={styles.cursorIcon}
                contentFit="cover"
                source={require("../assets/cursor.png")}
              />
            </View>
            <View style={[styles.send, styles.sendFlexBox]}>
              <View style={styles.icon}>
                <View style={[styles.fill1, styles.fillPosition]} />
              </View>
            </View>
          </Pressable>
        </View>
      </View>
      <View style={styles.groupChildPosition}>
        <View style={[styles.groupChild, styles.groupChildPosition]} />
        <View style={[styles.vectorParent, styles.groupItemLayout]}>
          <Image
            style={[styles.groupItem, styles.groupItemLayout]}
            contentFit="cover"
            source={require("../assets/vector-1.png")}
          />
          <View style={styles.groupInner} />
          <View style={[styles.rectangleView, styles.groupChildLayout]} />
          <View style={[styles.groupChild1, styles.groupChildLayout]} />
          <View style={[styles.groupChild2, styles.groupChildLayout]} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  frameParentPosition: {
    position: "absolute",
    left: 0,
  },
  sendFlexBox: {
    alignItems: "center",
    flexDirection: "row",
    overflow: "hidden",
  },
  fillPosition: {
    left: "0%",
    bottom: "0%",
    right: "0%",
    top: "0%",
    width: "100%",
    height: "100%",
    position: "absolute",
  },
  textFlexBox: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
  },
  groupChildPosition: {
    height: 45,
    width: 360,
    left: 0,
    bottom: 0,
    position: "absolute",
  },
  groupItemLayout: {
    height: 15,
    position: "absolute",
  },
  groupChildLayout: {
    width: 1,
    backgroundColor: Color.colorGray_300,
    height: 14,
    bottom: 1,
    position: "absolute",
  },
  fill: {
    backgroundColor: Color.colorOrchid_100,
  },
  add: {
    width: 16,
    height: 16,
    overflow: "hidden",
  },
  moreOptions: {
    padding: Padding.p_5xs,
    flexDirection: "row",
  },
  placeholder: {
    fontSize: FontSize.bodyBodyM_size,
    lineHeight: 20,
    fontFamily: FontFamily.bodyBodyS,
    color: Color.neutralDarkLightest,
    textAlign: "left",
  },
  cursorIcon: {
    width: 0,
    marginLeft: 1,
    display: "none",
    height: 16,
  },
  fill1: {
    backgroundColor: Color.neutralLightLightest,
  },
  icon: {
    width: 12,
    height: 12,
  },
  send: {
    borderRadius: Border.br_19xl,
    backgroundColor: Color.colorRoyalblue_100,
    width: 32,
    height: 32,
    justifyContent: "center",
    paddingHorizontal: Padding.p_16xl,
    paddingVertical: Padding.p_smi,
    marginLeft: 12,
    display: "none",
  },
  textInput: {
    borderRadius: Border.br_52xl,
    backgroundColor: Color.neutralLightLight,
    height: 40,
    paddingHorizontal: Padding.p_base,
    paddingVertical: Padding.p_5xs,
    marginLeft: 6,
    overflow: "hidden",
  },
  messageInput: {
    alignSelf: "stretch",
  },
  messageInputWrapper: {
    right: 1,
    bottom: 45,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    shadowOffset: {
      width: 0,
      height: -1,
    },
    shadowRadius: 10,
    elevation: 10,
    shadowOpacity: 1,
    height: 68,
    padding: Padding.p_base,
    backgroundColor: Color.neutralLightLightest,
    left: 0,
  },
  groupChild: {
    backgroundColor: Color.colorWhitesmoke_200,
  },
  groupItem: {
    left: 203,
    width: 9,
    bottom: 0,
  },
  groupInner: {
    left: 100,
    borderRadius: Border.br_9xs,
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    width: 14,
    height: 14,
    bottom: 1,
    position: "absolute",
  },
  rectangleView: {
    left: 0,
  },
  groupChild1: {
    left: 6,
  },
  groupChild2: {
    left: 12,
  },
  vectorParent: {
    bottom: 15,
    left: 74,
    width: 212,
  },
  frameParent: {
    right: 0,
    height: 113,
    left: 0,
    bottom: 0,
  },
});

export default SectionForm1;
