import * as React from "react";
import { StyleSheet, View, Pressable, Text } from "react-native";
import { Image } from "expo-image";
import { LinearGradient } from "expo-linear-gradient";
import { useNavigation } from "@react-navigation/native";
import { Padding, Border, Color, FontSize, FontFamily } from "../GlobalStyles";

const SectionMessages1 = () => {
  const navigation = useNavigation();

  return (
    <View style={[styles.rectangleParent, styles.groupChildPosition]}>
      <View style={[styles.groupChild, styles.groupChildPosition]} />
      <View style={styles.menu}>
        <Pressable
          style={styles.menuItem}
          onPress={() => navigation.navigate("HOME")}
        >
          <Image
            style={styles.icons}
            contentFit="cover"
            source={require("../assets/icons.png")}
          />
        </Pressable>
        <Pressable
          style={styles.menuItemSpaceBlock}
          onPress={() => navigation.navigate("NewsPromo")}
        >
          <Image
            style={styles.icons}
            contentFit="cover"
            source={require("../assets/icons1.png")}
          />
        </Pressable>
        <Pressable
          style={styles.menuItemSpaceBlock}
          onPress={() => navigation.navigate("ActivityHistory")}
        >
          <Image
            style={styles.icons2}
            contentFit="cover"
            source={require("../assets/icons2.png")}
          />
        </Pressable>
        <LinearGradient
          style={[styles.menuItem3, styles.menuItemSpaceBlock]}
          locations={[0, 1]}
          colors={["#e7a5ec", "#dd7de1"]}
        >
          <Image
            style={styles.icons}
            contentFit="cover"
            source={require("../assets/icons3.png")}
          />
          <Text style={styles.messages}>Messages</Text>
        </LinearGradient>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  groupChildPosition: {
    width: 360,
    left: 0,
    bottom: 0,
    position: "absolute",
  },
  menuItemSpaceBlock: {
    marginLeft: 19,
    paddingVertical: Padding.p_7xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    borderRadius: Border.br_13xl,
    flexDirection: "row",
  },
  groupChild: {
    backgroundColor: Color.colorWhitesmoke_200,
    height: 45,
  },
  icons: {
    width: 24,
    height: 24,
    overflow: "hidden",
  },
  menuItem: {
    paddingVertical: Padding.p_7xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    borderRadius: Border.br_13xl,
    flexDirection: "row",
  },
  icons2: {
    width: 26,
    height: 25,
    overflow: "hidden",
  },
  messages: {
    fontSize: FontSize.headingH5_size,
    lineHeight: 11,
    fontWeight: "700",
    fontFamily: FontFamily.gBoardTiny,
    color: Color.neutralLightLightest,
    textAlign: "left",
    marginLeft: 5,
  },
  menuItem3: {
    backgroundColor: "transparent",
  },
  menu: {
    marginLeft: -167.5,
    bottom: 71,
    left: "50%",
    borderRadius: Border.br_20xl,
    backgroundColor: Color.neutralLightLightest,
    shadowColor: "rgba(221, 125, 225, 0.25)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 15,
    elevation: 15,
    shadowOpacity: 1,
    paddingHorizontal: Padding.p_2xs,
    paddingVertical: Padding.p_5xs,
    flexDirection: "row",
    position: "absolute",
  },
  rectangleParent: {
    height: 124,
  },
});

export default SectionMessages1;
