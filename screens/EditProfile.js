import * as React from "react";
import { StyleSheet, View, Text } from "react-native";
import { Image } from "expo-image";
import { LinearGradient } from "expo-linear-gradient";
import UploadImageForm from "../components/UploadImageForm";
import { Color, FontSize, FontFamily, Border } from "../GlobalStyles";

const EditProfile = () => {
  return (
    <View style={styles.editProfile}>
      <View style={[styles.editProfileChild, styles.editPosition]} />
      <View style={[styles.vectorParent, styles.groupChildLayout]}>
        <Image
          style={[styles.groupChild, styles.groupChildLayout]}
          contentFit="cover"
          source={require("../assets/vector-1.png")}
        />
        <View style={styles.groupItem} />
        <View style={[styles.groupInner, styles.groupLayout]} />
        <View style={[styles.rectangleView, styles.groupLayout]} />
        <View style={[styles.groupChild1, styles.groupLayout]} />
      </View>
      <View style={[styles.editProfileItem, styles.editPosition]} />
      <LinearGradient
        style={[styles.editProfileInner, styles.iconPosition]}
        locations={[0, 1]}
        colors={["#f3f3f3", "#f8eef8"]}
      />
      <View style={[styles.rectangleParent, styles.groupChild2Layout]}>
        <View style={[styles.groupChild2, styles.groupChild2Layout]} />
        <Image
          style={[styles.vectorIcon, styles.iconPosition]}
          contentFit="cover"
          source={require("../assets/vector-3.png")}
        />
      </View>
      <View style={[styles.accountParent, styles.iconPosition]}>
        <Image
          style={[styles.accountIcon, styles.iconPosition]}
          contentFit="cover"
          source={require("../assets/account.png")}
        />
        <View style={[styles.nameParent, styles.parentPosition]}>
          <Text style={styles.nameTypo}>Name</Text>
          <Text style={[styles.text, styles.textTypo]}>*</Text>
        </View>
        <View style={[styles.phoneNumberParent, styles.parentPosition]}>
          <Text style={styles.nameTypo}>Phone Number</Text>
          <Text style={[styles.text, styles.textTypo]}>*</Text>
        </View>
        <View style={[styles.emailParent, styles.parentPosition]}>
          <Text style={styles.nameTypo}>Email</Text>
          <Text style={[styles.text, styles.textTypo]}>*</Text>
        </View>
        <Text style={[styles.changePassword, styles.nameTypo]}>
          Change Password
        </Text>
        <Image
          style={[styles.frameChild, styles.iconPosition]}
          contentFit="cover"
          source={require("../assets/vector-31.png")}
        />
        <UploadImageForm />
        <Text style={[styles.uploadImage, styles.textTypo]}>Upload Image</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  editPosition: {
    position: "absolute",
    width: 360,
    left: 0,
  },
  groupChildLayout: {
    height: 15,
    position: "absolute",
  },
  groupLayout: {
    width: 1,
    backgroundColor: Color.colorGray_300,
    height: 14,
    top: 0,
    position: "absolute",
  },
  iconPosition: {
    left: "50%",
    position: "absolute",
  },
  groupChild2Layout: {
    height: 26,
    width: 28,
    position: "absolute",
  },
  parentPosition: {
    flexDirection: "row",
    left: 14,
    position: "absolute",
  },
  textTypo: {
    color: Color.colorOrchid_100,
    textAlign: "left",
    lineHeight: 11,
    fontSize: FontSize.headingH5_size,
  },
  nameTypo: {
    textAlign: "left",
    color: Color.colorDimgray_100,
    lineHeight: 11,
    fontSize: FontSize.headingH5_size,
    fontFamily: FontFamily.gBoardLarge,
  },
  editProfileChild: {
    top: 755,
    backgroundColor: Color.colorWhitesmoke_200,
    height: 45,
    width: 360,
    left: 0,
  },
  groupChild: {
    left: 203,
    width: 9,
    top: 0,
  },
  groupItem: {
    left: 100,
    borderRadius: Border.br_9xs,
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    width: 14,
    height: 14,
    top: 0,
    position: "absolute",
  },
  groupInner: {
    left: 0,
  },
  rectangleView: {
    left: 6,
  },
  groupChild1: {
    left: 12,
  },
  vectorParent: {
    top: 771,
    left: 74,
    width: 212,
  },
  editProfileItem: {
    top: 703,
    backgroundColor: Color.colorGainsboro_200,
    height: 52,
    width: 360,
    left: 0,
  },
  editProfileInner: {
    marginLeft: -180,
    bottom: 0,
    borderTopLeftRadius: Border.br_7xl,
    borderTopRightRadius: Border.br_7xl,
    shadowColor: "rgba(0, 0, 0, 0.25)",
    shadowOffset: {
      width: 0,
      height: -20,
    },
    shadowRadius: 34,
    elevation: 34,
    shadowOpacity: 1,
    height: 734,
    backgroundColor: "transparent",
    width: 360,
  },
  groupChild2: {
    top: 0,
    left: 0,
  },
  vectorIcon: {
    marginLeft: -6.71,
    bottom: 5,
    height: 17,
    width: 9,
  },
  rectangleParent: {
    top: 86,
    left: 23,
  },
  accountIcon: {
    marginLeft: -48.5,
    bottom: 453,
    width: 96,
    height: 96,
  },
  text: {
    marginLeft: 2,
    fontFamily: FontFamily.gBoardLarge,
    color: Color.colorOrchid_100,
  },
  nameParent: {
    top: 181,
  },
  phoneNumberParent: {
    top: 219,
  },
  emailParent: {
    top: 260,
  },
  changePassword: {
    marginLeft: -137.5,
    bottom: 250,
    left: "50%",
    position: "absolute",
  },
  frameChild: {
    marginLeft: 130.13,
    bottom: 249,
    width: 7,
    height: 12,
  },
  uploadImage: {
    marginLeft: -37.5,
    top: 126,
    fontWeight: "600",
    fontFamily: FontFamily.gBoardTiny,
    left: "50%",
    position: "absolute",
  },
  accountParent: {
    marginLeft: -152,
    top: 115,
    width: 303,
    height: 564,
  },
  editProfile: {
    flex: 1,
    width: "100%",
    height: 800,
    overflow: "hidden",
  },
});

export default EditProfile;
