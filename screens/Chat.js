import * as React from "react";
import { StyleSheet, View, Text, Pressable } from "react-native";
import { Image } from "expo-image";
import { useNavigation } from "@react-navigation/native";
import SectionMessages1 from "../components/SectionMessages1";
import { Color, Padding, Border, FontFamily, FontSize } from "../GlobalStyles";

const Chat = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.chat}>
      <View style={styles.chatChild} />
      <View style={[styles.chatItem, styles.chatLayout]} />
      <View style={[styles.chatInner, styles.chatLayout]} />
      <View style={styles.groupChildPosition}>
        <View style={[styles.groupChild, styles.groupChildPosition]} />
        <Text style={[styles.text, styles.textClr]}>100%</Text>
        <Text style={[styles.text1, styles.textClr]}>00:00</Text>
        <Image
          style={styles.batteryIcon}
          contentFit="cover"
          source={require("../assets/battery-icon.png")}
        />
      </View>
      <View style={[styles.vectorParent, styles.groupItemLayout]}>
        <Image
          style={[styles.groupItem, styles.groupItemLayout]}
          contentFit="cover"
          source={require("../assets/vector-1.png")}
        />
        <View style={styles.groupInner} />
        <View style={[styles.rectangleView, styles.groupChildLayout]} />
        <View style={[styles.groupChild1, styles.groupChildLayout]} />
        <View style={[styles.groupChild2, styles.groupChildLayout]} />
      </View>
      <Text style={[styles.messages, styles.textClr]}>Messages</Text>
      <View style={[styles.contentSwitcher, styles.badgeFlexBox]}>
        <View style={[styles.section1, styles.sectionFlexBox]}>
          <Text style={[styles.section, styles.titleTypo]}>Chats</Text>
        </View>
        <Image
          style={styles.dividerIcon}
          contentFit="cover"
          source={require("../assets/divider.png")}
        />
        <Pressable
          style={[styles.section2, styles.sectionFlexBox]}
          onPress={() => navigation.navigate("Notifications")}
        >
          <Text style={[styles.section3, styles.section3Clr]}>
            Notifications
          </Text>
        </Pressable>
      </View>
      <Pressable
        style={[styles.listItem, styles.listItemSpaceBlock]}
        onPress={() => navigation.navigate("ChatPage2")}
      >
        <View style={styles.supportingVisuals}>
          <Image
            style={styles.avatarIcon}
            contentFit="cover"
            source={require("../assets/avatar.png")}
          />
        </View>
        <View style={styles.content}>
          <Text style={[styles.title, styles.titleTypo]}>Kevin Koopman</Text>
          <Text style={[styles.description, styles.section3Clr]}>
            Halo, saya belum sampai.
          </Text>
        </View>
        <View style={[styles.badge, styles.badgeFlexBox]}>
          <Text style={styles.text2}>4</Text>
        </View>
      </Pressable>
      <View style={[styles.listItem1, styles.listItemSpaceBlock]}>
        <View style={styles.supportingVisuals}>
          <Image
            style={styles.avatarIcon}
            contentFit="cover"
            source={require("../assets/avatar.png")}
          />
        </View>
        <View style={styles.content}>
          <Text style={[styles.title, styles.titleTypo]}>Retta</Text>
          <Text style={[styles.description, styles.section3Clr]}>Ok</Text>
        </View>
      </View>
      <SectionMessages1 />
    </View>
  );
};

const styles = StyleSheet.create({
  chatLayout: {
    display: "none",
    width: 16,
    borderWidth: 1,
    borderColor: Color.colorLime,
    borderStyle: "solid",
    top: 0,
    position: "absolute",
    height: 800,
  },
  groupChildPosition: {
    height: 33,
    left: 0,
    width: 360,
    top: 0,
    position: "absolute",
  },
  textClr: {
    color: Color.gBoard000000,
    textAlign: "left",
    position: "absolute",
  },
  groupItemLayout: {
    height: 15,
    position: "absolute",
  },
  groupChildLayout: {
    backgroundColor: Color.colorGray_300,
    width: 1,
    height: 14,
    top: 0,
    position: "absolute",
  },
  badgeFlexBox: {
    justifyContent: "center",
    alignItems: "center",
  },
  sectionFlexBox: {
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: Padding.p_xs,
    borderRadius: Border.br_xs,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    flex: 1,
  },
  titleTypo: {
    color: Color.neutralDarkDarkest,
    fontFamily: FontFamily.headingH5,
    fontWeight: "700",
    fontSize: FontSize.headingH5_size,
  },
  section3Clr: {
    color: Color.neutralDarkLight,
    fontSize: FontSize.headingH5_size,
  },
  listItemSpaceBlock: {
    paddingVertical: Padding.p_base,
    paddingHorizontal: Padding.p_xl,
    width: 359,
    alignItems: "center",
    flexDirection: "row",
    left: "50%",
    marginLeft: -180,
    position: "absolute",
  },
  chatChild: {
    shadowColor: "rgba(0, 0, 0, 0.07)",
    shadowOffset: {
      width: 0,
      height: -331,
    },
    shadowRadius: 94,
    elevation: 94,
    shadowOpacity: 1,
    height: 145,
    width: 360,
    marginLeft: -180,
    backgroundColor: Color.neutralLightLightest,
    left: "50%",
    top: 0,
    position: "absolute",
  },
  chatItem: {
    left: 344,
  },
  chatInner: {
    left: 0,
  },
  groupChild: {
    backgroundColor: Color.neutralLightLightest,
  },
  text: {
    left: 301,
    textAlign: "left",
    fontFamily: FontFamily.arial,
    fontSize: FontSize.headingH5_size,
    top: 13,
    color: Color.gBoard000000,
  },
  text1: {
    left: 20,
    textAlign: "left",
    fontFamily: FontFamily.arial,
    fontSize: FontSize.headingH5_size,
    top: 13,
    color: Color.gBoard000000,
  },
  batteryIcon: {
    height: "30.77%",
    width: "1.94%",
    top: "46.15%",
    right: "5.56%",
    bottom: "23.08%",
    left: "92.5%",
    maxWidth: "100%",
    maxHeight: "100%",
    position: "absolute",
    overflow: "hidden",
  },
  groupItem: {
    left: 203,
    width: 9,
    top: 0,
    height: 15,
  },
  groupInner: {
    left: 100,
    borderRadius: Border.br_9xs,
    borderColor: Color.colorGray_200,
    width: 14,
    height: 14,
    borderWidth: 1,
    borderStyle: "solid",
    top: 0,
    position: "absolute",
  },
  rectangleView: {
    width: 1,
    left: 0,
  },
  groupChild1: {
    left: 6,
    width: 1,
  },
  groupChild2: {
    left: 12,
    width: 1,
  },
  vectorParent: {
    top: 771,
    left: 74,
    width: 212,
  },
  messages: {
    marginLeft: -49,
    top: 61,
    fontSize: FontSize.size_xl,
    lineHeight: 18,
    fontFamily: FontFamily.arialRoundedMTBold,
    textAlign: "left",
    color: Color.gBoard000000,
    left: "50%",
  },
  section: {
    textAlign: "center",
  },
  section1: {
    zIndex: 2,
    backgroundColor: Color.neutralLightLightest,
  },
  dividerIcon: {
    height: 11,
    zIndex: 1,
    width: 1,
  },
  section3: {
    textAlign: "center",
    fontFamily: FontFamily.headingH5,
    fontWeight: "700",
    color: Color.neutralDarkLight,
  },
  section2: {
    zIndex: 0,
  },
  contentSwitcher: {
    marginLeft: -164,
    top: 97,
    backgroundColor: Color.colorGhostwhite_100,
    width: 328,
    padding: Padding.p_9xs,
    flexDirection: "row",
    alignItems: "center",
    borderRadius: Border.br_base,
    left: "50%",
    position: "absolute",
  },
  avatarIcon: {
    width: 40,
    height: 40,
    borderRadius: Border.br_base,
    overflow: "hidden",
  },
  supportingVisuals: {
    alignSelf: "stretch",
  },
  title: {
    alignSelf: "stretch",
    textAlign: "left",
  },
  description: {
    letterSpacing: 0.1,
    lineHeight: 16,
    fontFamily: FontFamily.bodyBodyS,
    marginTop: 4,
    alignSelf: "stretch",
    textAlign: "left",
  },
  content: {
    marginLeft: 16,
    flex: 1,
  },
  text2: {
    fontSize: FontSize.bodyBodyXS_size,
    letterSpacing: 0.5,
    textTransform: "uppercase",
    fontWeight: "600",
    fontFamily: FontFamily.actionActionM,
    color: Color.neutralLightLightest,
    textAlign: "center",
  },
  badge: {
    borderRadius: Border.br_xl,
    backgroundColor: Color.colorOrchid_100,
    width: 24,
    height: 24,
    padding: Padding.p_7xs,
    marginLeft: 16,
  },
  listItem: {
    top: 145,
  },
  listItem1: {
    top: 219,
  },
  chat: {
    backgroundColor: Color.colorGray_100,
    width: "100%",
    overflow: "hidden",
    height: 800,
    flex: 1,
  },
});

export default Chat;
