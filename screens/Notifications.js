import * as React from "react";
import { StyleSheet, View, Pressable, Text } from "react-native";
import { Image } from "expo-image";
import { LinearGradient } from "expo-linear-gradient";
import { useNavigation } from "@react-navigation/native";
import { Color, Padding, Border, FontFamily, FontSize } from "../GlobalStyles";

const Notifications = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.notifications}>
      <View style={styles.notificationsChild} />
      <View style={styles.vectorParent}>
        <Image
          style={styles.groupChild}
          contentFit="cover"
          source={require("../assets/vector-1.png")}
        />
        <View style={styles.groupItem} />
        <View style={[styles.groupInner, styles.groupLayout]} />
        <View style={[styles.rectangleView, styles.groupLayout]} />
        <View style={[styles.groupChild1, styles.groupLayout]} />
      </View>
      <View style={styles.menu}>
        <Pressable
          style={styles.menuItem}
          onPress={() => navigation.navigate("HOME")}
        >
          <Image
            style={styles.icons}
            contentFit="cover"
            source={require("../assets/icons.png")}
          />
        </Pressable>
        <Pressable
          style={styles.menuItemSpaceBlock}
          onPress={() => navigation.navigate("NewsPromo")}
        >
          <Image
            style={styles.icons}
            contentFit="cover"
            source={require("../assets/icons1.png")}
          />
        </Pressable>
        <Pressable
          style={styles.menuItemSpaceBlock}
          onPress={() => navigation.navigate("ActivityHistory")}
        >
          <Image
            style={styles.icons2}
            contentFit="cover"
            source={require("../assets/icons2.png")}
          />
        </Pressable>
        <LinearGradient
          style={[styles.menuItem3, styles.menuItemSpaceBlock]}
          locations={[0, 1]}
          colors={["#e7a5ec", "#dd7de1"]}
        >
          <Image
            style={styles.icons}
            contentFit="cover"
            source={require("../assets/icons3.png")}
          />
          <Text style={styles.messages}>Messages</Text>
        </LinearGradient>
      </View>
      <View style={[styles.listItem, styles.listItemLayout]}>
        <View style={styles.supportingVisuals}>
          <View style={styles.leftIcon}>
            <View style={styles.fill} />
          </View>
        </View>
        <View style={styles.content}>
          <Text style={styles.title}>Kilapartner Has Arrived</Text>
          <Text style={[styles.description, styles.hTypo]}>
            Description. Lorem ipsum dolor sit amet consectetur adipiscing elit,
            sed do
          </Text>
        </View>
        <Text style={[styles.h, styles.hTypo]}>59m</Text>
      </View>
      <View style={[styles.listItem1, styles.listItemLayout]}>
        <View style={styles.supportingVisuals}>
          <View style={styles.leftIcon}>
            <View style={styles.fill} />
          </View>
        </View>
        <View style={styles.content}>
          <Text style={styles.title}>Email Verification Success</Text>
          <Text style={[styles.description, styles.hTypo]}>
            Description. Lorem ipsum dolor sit amet consectetur adipiscing elit,
            sed do
          </Text>
        </View>
        <Text style={[styles.h, styles.hTypo]}>2h</Text>
      </View>
      <View style={[styles.listItem2, styles.listItemLayout]}>
        <View style={styles.supportingVisuals}>
          <View style={styles.leftIcon}>
            <View style={styles.fill} />
          </View>
        </View>
        <View style={styles.content}>
          <Text style={styles.title}>Appointment Success</Text>
          <Text style={[styles.description, styles.hTypo]}>
            Description. Lorem ipsum dolor sit amet consectetur adipiscing elit,
            sed do
          </Text>
        </View>
        <Text style={[styles.h, styles.hTypo]}>1h</Text>
      </View>
      <View style={styles.notificationsItem} />
      <View style={[styles.contentSwitcher, styles.sectionFlexBox]}>
        <Pressable
          style={[styles.section1, styles.sectionFlexBox]}
          onPress={() => navigation.navigate("Chat")}
        >
          <Text style={[styles.section, styles.sectionTypo]}>Chats</Text>
        </Pressable>
        <Image
          style={styles.dividerIcon}
          contentFit="cover"
          source={require("../assets/divider.png")}
        />
        <View style={[styles.section2, styles.sectionFlexBox]}>
          <Text style={[styles.section3, styles.sectionTypo]}>
            Notifications
          </Text>
        </View>
      </View>
      <View style={styles.groupChild2Position}>
        <View style={[styles.groupChild2, styles.groupChild2Position]} />
        <Text style={[styles.text, styles.textClr]}>100%</Text>
        <Text style={[styles.text1, styles.textClr]}>00:00</Text>
        <Image
          style={styles.batteryIcon}
          contentFit="cover"
          source={require("../assets/battery-icon.png")}
        />
      </View>
      <Text style={[styles.notifications1, styles.textClr]}>Notifications</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  groupLayout: {
    backgroundColor: Color.colorGray_300,
    width: 1,
    height: 14,
    top: 0,
    position: "absolute",
  },
  menuItemSpaceBlock: {
    marginLeft: 19,
    paddingVertical: Padding.p_7xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    borderRadius: Border.br_13xl,
    flexDirection: "row",
  },
  listItemLayout: {
    padding: Padding.p_base,
    width: 327,
    borderRadius: Border.br_xs,
    flexDirection: "row",
    left: "50%",
    position: "absolute",
  },
  hTypo: {
    fontFamily: FontFamily.bodyBodyS,
    lineHeight: 16,
    letterSpacing: 0.1,
    color: Color.neutralDarkLight,
    textAlign: "left",
    fontSize: FontSize.headingH5_size,
  },
  sectionFlexBox: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  sectionTypo: {
    textAlign: "center",
    fontFamily: FontFamily.headingH5,
    fontWeight: "700",
    fontSize: FontSize.headingH5_size,
  },
  groupChild2Position: {
    height: 33,
    top: 0,
    width: 360,
    left: 0,
    position: "absolute",
  },
  textClr: {
    color: Color.gBoard000000,
    textAlign: "left",
    position: "absolute",
  },
  notificationsChild: {
    top: 755,
    backgroundColor: Color.colorWhitesmoke_200,
    height: 45,
    width: 360,
    left: 0,
    position: "absolute",
  },
  groupChild: {
    left: 203,
    width: 9,
    top: 0,
    height: 15,
    position: "absolute",
  },
  groupItem: {
    left: 100,
    borderRadius: Border.br_9xs,
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    width: 14,
    height: 14,
    top: 0,
    position: "absolute",
  },
  groupInner: {
    width: 1,
    left: 0,
    backgroundColor: Color.colorGray_300,
  },
  rectangleView: {
    left: 6,
    width: 1,
  },
  groupChild1: {
    left: 12,
    width: 1,
  },
  vectorParent: {
    top: 771,
    left: 74,
    width: 212,
    height: 15,
    position: "absolute",
  },
  icons: {
    width: 24,
    height: 24,
    overflow: "hidden",
  },
  menuItem: {
    paddingVertical: Padding.p_7xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    borderRadius: Border.br_13xl,
    flexDirection: "row",
  },
  icons2: {
    width: 26,
    height: 25,
    overflow: "hidden",
  },
  messages: {
    lineHeight: 11,
    fontFamily: FontFamily.gBoardTiny,
    color: Color.neutralLightLightest,
    marginLeft: 5,
    textAlign: "left",
    fontWeight: "700",
    fontSize: FontSize.headingH5_size,
  },
  menuItem3: {
    backgroundColor: "transparent",
  },
  menu: {
    marginLeft: -167.5,
    top: 676,
    borderRadius: Border.br_20xl,
    shadowColor: "rgba(221, 125, 225, 0.25)",
    shadowRadius: 15,
    elevation: 15,
    paddingHorizontal: Padding.p_2xs,
    paddingVertical: Padding.p_5xs,
    flexDirection: "row",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    backgroundColor: Color.neutralLightLightest,
    left: "50%",
    position: "absolute",
  },
  fill: {
    height: "100%",
    top: "0%",
    right: "0%",
    bottom: "0%",
    left: "0%",
    backgroundColor: Color.colorMediumorchid,
    position: "absolute",
    width: "100%",
  },
  leftIcon: {
    height: 20,
    width: 20,
    overflow: "hidden",
  },
  supportingVisuals: {
    width: 20,
    alignSelf: "stretch",
  },
  title: {
    fontSize: FontSize.bodyBodyM_size,
    lineHeight: 20,
    fontWeight: "500",
    fontFamily: FontFamily.interMedium,
    color: Color.neutralDarkDarkest,
    alignSelf: "stretch",
    textAlign: "left",
  },
  description: {
    marginTop: 4,
    color: Color.neutralDarkLight,
    alignSelf: "stretch",
  },
  content: {
    marginLeft: 16,
    flex: 1,
  },
  h: {
    color: Color.neutralDarkLight,
    marginLeft: 16,
  },
  listItem: {
    top: 153,
    backgroundColor: Color.colorGhostwhite_100,
    marginLeft: -164,
  },
  listItem1: {
    top: 357,
    marginLeft: -164,
    padding: Padding.p_base,
    width: 327,
    backgroundColor: Color.neutralLightLightest,
  },
  listItem2: {
    marginLeft: -163,
    top: 263,
    backgroundColor: Color.neutralLightLightest,
  },
  notificationsItem: {
    marginLeft: -180,
    shadowColor: "rgba(0, 0, 0, 0.07)",
    shadowRadius: 94,
    elevation: 94,
    height: 145,
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    backgroundColor: Color.neutralLightLightest,
    left: "50%",
    top: 0,
    width: 360,
    position: "absolute",
  },
  section: {
    color: Color.neutralDarkLight,
  },
  section1: {
    zIndex: 2,
    borderRadius: Border.br_xs,
    justifyContent: "center",
    paddingHorizontal: Padding.p_xs,
    paddingVertical: Padding.p_5xs,
    flex: 1,
  },
  dividerIcon: {
    height: 11,
    zIndex: 1,
    width: 1,
  },
  section3: {
    color: Color.neutralDarkDarkest,
  },
  section2: {
    zIndex: 0,
    borderRadius: Border.br_xs,
    justifyContent: "center",
    paddingHorizontal: Padding.p_xs,
    paddingVertical: Padding.p_5xs,
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
  },
  contentSwitcher: {
    top: 97,
    borderRadius: Border.br_base,
    width: 328,
    padding: Padding.p_9xs,
    backgroundColor: Color.colorGhostwhite_100,
    marginLeft: -164,
    left: "50%",
    position: "absolute",
  },
  groupChild2: {
    backgroundColor: Color.neutralLightLightest,
  },
  text: {
    left: 301,
    fontFamily: FontFamily.arial,
    top: 13,
    color: Color.gBoard000000,
    fontSize: FontSize.headingH5_size,
  },
  text1: {
    left: 20,
    fontFamily: FontFamily.arial,
    top: 13,
    color: Color.gBoard000000,
    fontSize: FontSize.headingH5_size,
  },
  batteryIcon: {
    height: "30.77%",
    width: "1.94%",
    top: "46.15%",
    right: "5.56%",
    bottom: "23.08%",
    left: "92.5%",
    maxWidth: "100%",
    maxHeight: "100%",
    position: "absolute",
    overflow: "hidden",
  },
  notifications1: {
    marginLeft: -62,
    top: 61,
    fontSize: FontSize.size_xl,
    lineHeight: 18,
    fontFamily: FontFamily.arialRoundedMTBold,
    left: "50%",
  },
  notifications: {
    backgroundColor: Color.colorGray_100,
    height: 800,
    overflow: "hidden",
    width: "100%",
    flex: 1,
  },
});

export default Notifications;
