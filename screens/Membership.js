import * as React from "react";
import { StyleSheet, View, Text } from "react-native";
import { Image } from "expo-image";
import { LinearGradient } from "expo-linear-gradient";
import { Color, FontFamily, FontSize, Border } from "../GlobalStyles";

const Membership = () => {
  return (
    <LinearGradient
      style={styles.membership}
      locations={[0, 0.32, 0.59]}
      colors={["#b4e3e0", "#68c7c1", "#214e4b"]}
    >
      <View style={[styles.membershipChild, styles.membershipPosition]} />
      <Text style={[styles.text, styles.textFlexBox]}>100%</Text>
      <Text style={[styles.text1, styles.textFlexBox]}>00:00</Text>
      <Image
        style={[styles.batteryIcon, styles.iconLayout]}
        contentFit="cover"
        source={require("../assets/battery-icon2.png")}
      />
      <View style={[styles.membershipItem, styles.membershipPosition]} />
      <View style={[styles.vectorParent, styles.groupChildLayout]}>
        <Image
          style={[styles.groupChild, styles.groupChildLayout]}
          contentFit="cover"
          source={require("../assets/vector-1.png")}
        />
        <View style={styles.groupItem} />
        <View style={[styles.groupInner, styles.groupLayout]} />
        <View style={[styles.rectangleView, styles.groupLayout]} />
        <View style={[styles.groupChild1, styles.groupLayout]} />
      </View>
      <Image
        style={[styles.vectorIcon, styles.iconLayout]}
        contentFit="cover"
        source={require("../assets/vector1.png")}
      />
      <View style={[styles.kilapeepsSince, styles.kilapeepsLayout]}>
        <View style={[styles.kilapeepsSinceChild, styles.kilapeepsLayout]} />
        <Text style={[styles.kilapeepsSince1, styles.totalOrder1Typo]}>
          Kilapeeps Since
        </Text>
        <Text style={[styles.august2023, styles.text2Typo]}>August 2023</Text>
      </View>
      <View style={[styles.totalOrder, styles.kilapeepsLayout]}>
        <View style={[styles.kilapeepsSinceChild, styles.kilapeepsLayout]} />
        <Text style={[styles.text2, styles.text2Typo]}>100</Text>
        <Text style={[styles.totalOrder1, styles.xpPosition]}>Total Order</Text>
      </View>
      <View style={styles.membershipStatus}>
        <View
          style={[styles.membershipStatusChild, styles.membershipLayout1]}
        />
        <LinearGradient
          style={[styles.membershipStatusItem, styles.membershipLayout1]}
          locations={[0, 1]}
          colors={["#214e4b", "#68c7c1"]}
        />
        <Text style={[styles.xp, styles.xpPosition]}>4.000/5.000 XP</Text>
      </View>
      <View style={[styles.membershipInner, styles.membershipLayout]} />
      <View style={[styles.membershipChild1, styles.membershipLayout]} />
      <Image
        style={[styles.rectangleIcon, styles.membershipPosition]}
        contentFit="cover"
        source={require("../assets/rectangle-309.png")}
      />
      <Image
        style={styles.kilapinMedal3x2Icon}
        contentFit="cover"
        source={require("../assets/kilapin-medal3x-2.png")}
      />
      <Text style={[styles.oswinPrasetio, styles.rewardsTypo]}>
        OSWIN PRASETIO
      </Text>
      <Text style={styles.splendid}>Splendid</Text>
      <Text style={[styles.rewards, styles.rewardsTypo]}>Rewards</Text>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  membershipPosition: {
    width: 360,
    left: 0,
    position: "absolute",
  },
  textFlexBox: {
    textAlign: "left",
    position: "absolute",
  },
  iconLayout: {
    maxHeight: "100%",
    maxWidth: "100%",
    position: "absolute",
    overflow: "hidden",
  },
  groupChildLayout: {
    height: 15,
    position: "absolute",
  },
  groupLayout: {
    width: 1,
    backgroundColor: Color.colorGray_300,
    height: 14,
    top: 0,
    position: "absolute",
  },
  kilapeepsLayout: {
    height: 48,
    width: 150,
    position: "absolute",
  },
  totalOrder1Typo: {
    color: Color.colorDimgray_100,
    fontFamily: FontFamily.gBoardLarge,
    left: 15,
    lineHeight: 11,
    fontSize: FontSize.headingH5_size,
  },
  text2Typo: {
    color: Color.gBoard000000,
    fontWeight: "700",
    lineHeight: 13,
    fontSize: FontSize.bodyBodyM_size,
    fontFamily: FontFamily.gBoardTiny,
    left: 15,
    textAlign: "left",
    position: "absolute",
  },
  xpPosition: {
    top: 10,
    textAlign: "left",
    position: "absolute",
  },
  membershipLayout1: {
    borderRadius: Border.br_11xl,
    height: 28,
    top: 0,
    position: "absolute",
  },
  membershipLayout: {
    width: 16,
    borderColor: Color.colorLime,
    borderWidth: 1,
    borderStyle: "solid",
    top: 0,
    position: "absolute",
    height: 800,
  },
  rewardsTypo: {
    lineHeight: 18,
    fontSize: FontSize.size_xl,
    textAlign: "left",
    color: Color.neutralLightLightest,
    position: "absolute",
  },
  membershipChild: {
    backgroundColor: Color.colorGainsboro_200,
    height: 33,
    top: 0,
  },
  text: {
    left: 301,
    color: Color.neutralLightLightest,
    fontFamily: FontFamily.arial,
    fontSize: FontSize.headingH5_size,
    top: 13,
    textAlign: "left",
  },
  text1: {
    left: 20,
    color: Color.neutralLightLightest,
    fontFamily: FontFamily.arial,
    fontSize: FontSize.headingH5_size,
    top: 13,
    textAlign: "left",
  },
  batteryIcon: {
    height: "1.25%",
    width: "1.94%",
    top: "1.88%",
    right: "5.56%",
    bottom: "96.88%",
    left: "92.5%",
  },
  membershipItem: {
    top: 755,
    backgroundColor: Color.colorWhitesmoke_200,
    height: 45,
  },
  groupChild: {
    left: 203,
    width: 9,
    top: 0,
  },
  groupItem: {
    left: 100,
    borderRadius: Border.br_9xs,
    borderColor: Color.colorGray_200,
    width: 14,
    height: 14,
    borderWidth: 1,
    borderStyle: "solid",
    top: 0,
    position: "absolute",
  },
  groupInner: {
    left: 0,
  },
  rectangleView: {
    left: 6,
  },
  groupChild1: {
    left: 12,
  },
  vectorParent: {
    top: 771,
    left: 74,
    width: 212,
  },
  vectorIcon: {
    height: "2%",
    width: "4.44%",
    top: "6.5%",
    right: "7.78%",
    bottom: "91.5%",
    left: "87.78%",
  },
  kilapeepsSinceChild: {
    borderRadius: Border.br_mini,
    backgroundColor: Color.neutralLightLightest,
    left: 0,
    top: 0,
  },
  kilapeepsSince1: {
    top: 9,
    textAlign: "left",
    position: "absolute",
  },
  august2023: {
    top: 25,
  },
  kilapeepsSince: {
    left: 27,
    top: 327,
    width: 150,
  },
  text2: {
    top: 26,
  },
  totalOrder1: {
    color: Color.colorDimgray_100,
    fontFamily: FontFamily.gBoardLarge,
    left: 15,
    lineHeight: 11,
    fontSize: FontSize.headingH5_size,
  },
  totalOrder: {
    left: 182,
    top: 327,
    width: 150,
  },
  membershipStatusChild: {
    backgroundColor: Color.colorGainsboro_100,
    width: 280,
    left: "50%",
    marginLeft: -140,
    borderRadius: Border.br_11xl,
  },
  membershipStatusItem: {
    width: 225,
    left: 0,
    backgroundColor: "transparent",
  },
  xp: {
    left: 77,
    fontSize: FontSize.bodyBodyXS_size,
    lineHeight: 9,
    fontWeight: "600",
    fontFamily: FontFamily.gBoardTiny,
    top: 10,
    color: Color.neutralLightLightest,
  },
  membershipStatus: {
    top: 274,
    height: 28,
    width: 280,
    left: "50%",
    marginLeft: -140,
    position: "absolute",
  },
  membershipInner: {
    left: 344,
  },
  membershipChild1: {
    left: 0,
  },
  rectangleIcon: {
    top: 68,
    borderRadius: Border.br_5xl,
    height: 196,
  },
  kilapinMedal3x2Icon: {
    top: 66,
    left: 186,
    width: 174,
    height: 193,
    position: "absolute",
  },
  oswinPrasetio: {
    top: 110,
    fontFamily: FontFamily.arialRoundedMTBold,
    left: 34,
  },
  splendid: {
    top: 134,
    left: 34,
    lineHeight: 11,
    textAlign: "left",
    color: Color.neutralLightLightest,
    fontFamily: FontFamily.arial,
    fontSize: FontSize.headingH5_size,
    position: "absolute",
  },
  rewards: {
    top: 413,
    left: 40,
    fontWeight: "900",
    fontFamily: FontFamily.robotoBlack,
  },
  membership: {
    flex: 1,
    width: "100%",
    backgroundColor: "transparent",
    overflow: "hidden",
    height: 800,
  },
});

export default Membership;
