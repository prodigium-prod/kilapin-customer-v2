import * as React from "react";
import { StyleSheet, View, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Image } from "expo-image";
import EmptySectionCard from "../components/EmptySectionCard";
import ActivitySection from "../components/ActivitySection";
import { Color, FontFamily, FontSize, Padding, Border } from "../GlobalStyles";

const ActivityHistory = () => {
  return (
    <View style={styles.activityHistory}>
      <View style={[styles.activityHistoryChild, styles.historyLayout]} />
      <View style={[styles.history, styles.historyLayout]}>
        <EmptySectionCard />
      </View>
      <ActivitySection />
      <View style={styles.activityHistoryItem} />
      <View style={styles.frameParent}>
        <View style={[styles.groupParent, styles.notifPosition]}>
          <View style={styles.rectangleParent}>
            <LinearGradient
              style={styles.groupChild}
              locations={[0, 1]}
              colors={["#dd7de1", "rgba(221, 125, 225, 0.54)"]}
            />
            <Text style={[styles.history1, styles.onGoingLayout]}>History</Text>
          </View>
          <View style={styles.rectangleGroup}>
            <View style={styles.groupItem} />
            <Text style={[styles.onGoing, styles.onGoingTypo]}>On going</Text>
          </View>
          <View style={styles.rectangleGroup}>
            <View style={styles.groupItem} />
            <Text style={[styles.scheduled, styles.onGoingTypo]}>
              Scheduled
            </Text>
          </View>
        </View>
        <View style={styles.contentSwitcher}>
          <View style={[styles.section1, styles.sectionFlexBox]}>
            <Text style={[styles.section, styles.sectionTypo]}>History</Text>
          </View>
          <Image
            style={[styles.dividerIcon, styles.dividerIconLayout]}
            contentFit="cover"
            source={require("../assets/divider1.png")}
          />
          <View style={[styles.section2, styles.sectionFlexBox]}>
            <Text style={[styles.section3, styles.sectionTypo]}>On Going</Text>
          </View>
          <Image
            style={[styles.dividerIcon1, styles.dividerIconLayout]}
            contentFit="cover"
            source={require("../assets/divider2.png")}
          />
          <View style={[styles.section31, styles.sectionFlexBox]}>
            <Text style={[styles.section3, styles.sectionTypo]}>Scheduled</Text>
          </View>
        </View>
      </View>
      <Text style={styles.activity}>Activity</Text>
      <View style={[styles.notifBar, styles.notifPosition1]}>
        <View style={[styles.notifBarChild, styles.notifPosition]} />
        <View style={[styles.notifBarItem, styles.notifPosition]} />
        <View style={[styles.notifBarInner, styles.notifBarInnerLayout]} />
        <View style={[styles.rectangleView, styles.notifBarInnerLayout]} />
        <Text style={[styles.text, styles.textTypo]}>100%</Text>
        <Text style={[styles.text1, styles.textTypo]}>00:00</Text>
        <Image
          style={styles.batteryIcon}
          contentFit="cover"
          source={require("../assets/battery-icon.png")}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  historyLayout: {
    width: 360,
    left: 0,
  },
  notifPosition: {
    display: "none",
    position: "absolute",
  },
  onGoingLayout: {
    height: 15,
    top: 7,
  },
  onGoingTypo: {
    color: Color.colorOrchid_100,
    textAlign: "center",
    fontFamily: FontFamily.gBoardLarge,
    fontSize: FontSize.headingH5_size,
    position: "absolute",
  },
  sectionFlexBox: {
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: Padding.p_xs,
    borderRadius: Border.br_xs,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    flex: 1,
  },
  sectionTypo: {
    fontFamily: FontFamily.headingH5,
    fontWeight: "700",
    textAlign: "center",
    fontSize: FontSize.headingH5_size,
  },
  dividerIconLayout: {
    height: 11,
    width: 1,
  },
  notifPosition1: {
    top: 0,
    width: 360,
    left: 0,
  },
  notifBarInnerLayout: {
    width: 16,
    borderColor: Color.colorLime,
    borderWidth: 1,
    borderStyle: "solid",
    top: 0,
    position: "absolute",
    height: 800,
  },
  textTypo: {
    fontFamily: FontFamily.arial,
    top: 13,
    textAlign: "left",
    color: Color.gBoard000000,
    fontSize: FontSize.headingH5_size,
    position: "absolute",
  },
  activityHistoryChild: {
    top: 703,
    backgroundColor: Color.neutralLightLight,
    height: 52,
    position: "absolute",
  },
  history: {
    top: 83,
    height: 676,
    position: "absolute",
  },
  activityHistoryItem: {
    marginLeft: -180,
    shadowColor: "rgba(0, 0, 0, 0.07)",
    shadowOffset: {
      width: 0,
      height: -331,
    },
    shadowRadius: 94,
    elevation: 94,
    shadowOpacity: 1,
    height: 146,
    backgroundColor: Color.neutralLightLightest,
    left: "50%",
    top: 0,
    width: 360,
    position: "absolute",
  },
  groupChild: {
    marginLeft: -48,
    backgroundColor: "transparent",
    borderRadius: Border.br_21xl,
    height: 28,
    width: 96,
    left: "50%",
    top: 0,
    position: "absolute",
  },
  history1: {
    left: 26,
    color: Color.neutralLightLightest,
    width: 43,
    textAlign: "center",
    fontFamily: FontFamily.gBoardLarge,
    fontSize: FontSize.headingH5_size,
    height: 15,
    top: 7,
    position: "absolute",
  },
  rectangleParent: {
    height: 28,
    width: 96,
  },
  groupItem: {
    marginLeft: -48.5,
    borderColor: Color.colorOrchid_100,
    borderWidth: 1,
    borderStyle: "solid",
    width: 97,
    borderRadius: Border.br_21xl,
    height: 28,
    left: "50%",
    top: 0,
    position: "absolute",
  },
  onGoing: {
    left: 24,
    width: 49,
    height: 15,
    top: 7,
    color: Color.colorOrchid_100,
  },
  rectangleGroup: {
    marginLeft: 10,
    width: 97,
    height: 28,
  },
  scheduled: {
    top: 6,
    left: 19,
    width: 59,
    height: 16,
  },
  groupParent: {
    marginLeft: -155,
    top: 5,
    justifyContent: "center",
    flexDirection: "row",
    display: "none",
    left: "50%",
  },
  section: {
    color: Color.neutralDarkDarkest,
  },
  section1: {
    zIndex: 4,
    backgroundColor: Color.neutralLightLightest,
  },
  dividerIcon: {
    zIndex: 3,
  },
  section3: {
    color: Color.neutralDarkLight,
  },
  section2: {
    zIndex: 2,
  },
  dividerIcon1: {
    zIndex: 1,
  },
  section31: {
    zIndex: 0,
  },
  contentSwitcher: {
    borderRadius: Border.br_base,
    backgroundColor: Color.colorGhostwhite_100,
    padding: Padding.p_9xs,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    width: 328,
    top: 0,
    left: 0,
    position: "absolute",
  },
  frameParent: {
    marginLeft: -164,
    top: 96,
    height: 39,
    width: 328,
    left: "50%",
    position: "absolute",
  },
  activity: {
    marginLeft: -37,
    top: 61,
    fontSize: FontSize.size_xl,
    lineHeight: 18,
    fontFamily: FontFamily.arialRoundedMTBold,
    textAlign: "left",
    color: Color.gBoard000000,
    fontWeight: "700",
    left: "50%",
    position: "absolute",
  },
  notifBarChild: {
    backgroundColor: Color.colorGainsboro_200,
    height: 33,
    top: 0,
    width: 360,
    left: 0,
  },
  notifBarItem: {
    top: 33,
    backgroundColor: Color.colorWhitesmoke_100,
    height: 56,
    width: 360,
    left: 0,
  },
  notifBarInner: {
    left: 344,
  },
  rectangleView: {
    left: 0,
    width: 16,
    borderColor: Color.colorLime,
  },
  text: {
    left: 301,
  },
  text1: {
    left: 20,
  },
  batteryIcon: {
    height: "1.25%",
    width: "1.94%",
    top: "1.88%",
    right: "5.56%",
    bottom: "96.88%",
    left: "92.5%",
    maxWidth: "100%",
    maxHeight: "100%",
    position: "absolute",
    overflow: "hidden",
  },
  notifBar: {
    position: "absolute",
    height: 800,
  },
  activityHistory: {
    backgroundColor: Color.colorGray_100,
    width: "100%",
    overflow: "hidden",
    height: 800,
    flex: 1,
  },
});

export default ActivityHistory;
