import * as React from "react";
import { Image } from "expo-image";
import { StyleSheet, View, Text, Pressable } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { useNavigation } from "@react-navigation/native";
import ActiveItemhome from "../components/ActiveItemhome";
import WelcomeHomeCard from "../components/WelcomeHomeCard";
import UpholsteryCareContainer from "../components/UpholsteryCareContainer";
import PromoCodeForm1 from "../components/PromoCodeForm1";
import { Color, FontFamily, FontSize, Border } from "../GlobalStyles";

const HOME = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.home}>
      <Image
        style={styles.home231}
        contentFit="cover"
        source={require("../assets/home-23-1.png")}
      />
      <View style={[styles.homeChild, styles.homeLayout]} />
      <View style={[styles.homeItem, styles.homeLayout]} />
      <View style={styles.promoCode}>
        <Image
          style={styles.frameIcon}
          contentFit="cover"
          source={require("../assets/frame.png")}
        />
        <View style={[styles.frame, styles.framePosition1]}>
          <View style={[styles.frameChild, styles.framePosition1]} />
          <Text style={styles.gotPromoCode}>Got promo code?</Text>
        </View>
      </View>
      <View style={[styles.topBar, styles.topBarSpaceBlock]}>
        <Text style={[styles.text, styles.textTypo]}>00:00</Text>
        <View style={styles.frame1}>
          <Image
            style={[styles.batteryIcon, styles.frame2Layout]}
            contentFit="cover"
            source={require("../assets/battery-icon1.png")}
          />
          <Text style={[styles.text1, styles.textTypo]}>100%</Text>
        </View>
      </View>
      <View style={[styles.homeInner, styles.homeInnerPosition]}>
        <View style={[styles.frameParent, styles.frameLayout1]}>
          <View style={[styles.frame2, styles.frame2Layout]}>
            <View style={[styles.frameItem, styles.framePosition]} />
            <View style={[styles.frameInner, styles.framePosition]} />
          </View>
          <View style={[styles.frame3, styles.frameLayout1]}>
            <Image
              style={[styles.vectorIcon, styles.homeInnerPosition]}
              contentFit="cover"
              source={require("../assets/vector-1.png")}
            />
            <View style={styles.frame4}>
              <View style={[styles.rectangleView, styles.framePosition]} />
              <View style={[styles.frameChild1, styles.homeInnerPosition]} />
            </View>
          </View>
        </View>
      </View>
      <ActiveItemhome
          icons={require("../assets/icons8.png")}
          icons1={require("../assets/icons1.png")}
          icons2={require("../assets/icons2.png")}
          icons3={require("../assets/icons5.png")}
          activeItemhomePosition="absolute"
          activeItemhomeElevation={15}
          activeItemhomeMarginLeft={-155.5}
          activeItemhomeBottom={71}
          activeItemhomeLeft="50%"
          onMenuItemPress={() => {
            console.log("onMenuItemPress for component 1 clicked");
            // navigation.navigate("NewsPromo");
          }}
          onMenuItemPress1={() => {
            console.log("onMenuItemPress1 for component 1 clicked");
            // navigation.navigate("ActivityHistory");
          }}
          onMenuItemPress2={() => {
            console.log("onMenuItemPress2 for component 1 clicked");
            // navigation.navigate("Chat");
          }}
      />
      <View style={[styles.frameGroup, styles.framePosition2]}>
        <WelcomeHomeCard />
        <View style={[styles.frame5, styles.framePosition2]}>
          <View style={styles.frameContainer}>
            <View style={[styles.frameView, styles.frameLayout]}>
              <View style={styles.spotlessFasterAndHassleFParent}>
                <Text
                  style={[styles.spotlessFasterAnd, styles.andTypo]}>{`Spotless, faster, and hassle-free`}</Text>
                <Text style={[styles.generalCleaning, styles.cleaningTypo, { color: 'white' }]}>{`General Cleaning`}</Text>
              </View>
              <View style={[styles.frame6, styles.frameLayout]}>
                <LinearGradient
                  style={[
                    styles.rectangleLineargradient,
                    styles.rectangleLayout,
                  ]}
                  locations={[0, 1]}
                  colors={["rgba(221, 125, 225, 0.39)", "#dd7de1"]}
                />
                <Image
                  style={styles.rectangleLayout}
                  contentFit="cover"
                  source={require("../assets/rectangle-17.png")}
                />
              </View>
            </View>
            <View style={styles.frameParentShadowBox}>
              <View
                style={[
                  styles.tacklesDirtStainsAndBactParent,
                  styles.parentLayout,
                ]}
              >
                <Text style={[styles.tacklesDirtStains, styles.andTypo]}>
                  Tackles dirt, stains, and bacteria in tough spots
                </Text>
                <Text style={[styles.deepCleaning, styles.cleaningTypo]}>{`Deep Cleaning`}</Text>
              </View>
              <View style={[styles.frame6, styles.frameLayout]}>
                <LinearGradient
                  style={[
                    styles.rectangleLineargradient,
                    styles.rectangleLayout,
                  ]}
                  locations={[0, 1]}
                  colors={["rgba(221, 125, 225, 0.39)", "#dd7de1"]}
                />
                <Image
                  style={styles.rectangleLayout}
                  contentFit="cover"
                  source={require("../assets/rectangle-171.png")}
                />
              </View>
            </View>
          </View>
          <View style={styles.frame11}>
            <Text style={styles.whatAreYou}>What are you looking for?</Text>
          </View>
          <PromoCodeForm1 />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  homeLayout: {
    height: 800,
    width: 16,
    borderColor: Color.colorLime,
    borderWidth: 1,
    borderStyle: "solid",
    top: 0,
    display: "none",
    position: "absolute",
  },
  framePosition1: {
    height: 44,
    left: "50%",
    top: 0,
    position: "absolute",
  },
  topBarSpaceBlock: {
    marginLeft: -180,
    width: 360,
  },
  textTypo: {
    color: Color.gBoard000000,
    fontFamily: FontFamily.arial,
    textAlign: "left",
    fontSize: FontSize.headingH5_size,
    left: "50%",
    position: "absolute",
  },
  frame2Layout: {
    width: 7,
    position: "absolute",
  },
  homeInnerPosition: {
    bottom: 0,
    left: "50%",
    position: "absolute",
  },
  frameLayout1: {
    height: 15,
    position: "absolute"
  },
  framePosition: {
    width: 1,
    backgroundColor: Color.colorGray_300,
    bottom: 0,
    height: 14,
    left: "50%",
    position: "absolute",
  },
  framePosition2: {
    width: 360,
    left: 0,
    position: "absolute",
  },
  frameLayout: {
    height: 214,
    width: 146,
  },
  andTypo: {
    height: 24,
    color: Color.neutralLightLightest,
    fontFamily: FontFamily.bodyBodyS,
    fontSize: FontSize.bodyBodyXS_size,
    width: 112,
    textAlign: "left",
    left: 0,
    position: "absolute",
  },
  cleaningTypo: {
    fontFamily: FontFamily.headingH5,
    fontWeight: "700",
    fontSize: FontSize.size_base,
    color: '#FFFFFF',
    textAlign: "left",
    top: 0,
    left: 0,
    position: "absolute"
  },
  rectangleLayout: {
    borderRadius: Border.br_sm,
    height: 214,
    width: 146,
    top: 0,
    left: 0,
    position: "absolute",
  },
  parentLayout: {
    height: 65,
    width: 112,
    top: 129,
    position: "absolute"
  },
  parentPosition: {
    left: 16,
    top: 111,
    width: 112,
    position: "absolute",
  },
  home231: {
    top: 316,
    height: 1415,
    display: "none",
    width: 360,
    left: 0,
    position: "absolute",
  },
  homeChild: {
    left: 344,
  },
  homeItem: {
    left: 0,
  },
  frameIcon: {
    top: 108,
    width: 292,
    height: 18,
    overflow: "hidden",
    left: 0,
    position: "absolute",
  },
  frameChild: {
    marginLeft: 105,
    borderTopRightRadius: Border.br_xs,
    borderBottomRightRadius: Border.br_xs,
    backgroundColor: "rgba(221, 125, 225, 0.87)",
    width: 48,
  },
  gotPromoCode: {
    top: 15,
    left: 21,
    fontWeight: "500",
    fontFamily: FontFamily.gBoardDefault,
    color: "#999",
    textAlign: "left",
    fontSize: FontSize.headingH5_size,
    position: "absolute",
  },
  frame: {
    marginLeft: -153,
    borderRadius: Border.br_xs,
    overflow: "hidden",
    width: 306,
    backgroundColor: Color.neutralLightLightest,
  },
  promoCode: {
    top: 427,
    left: 27,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    height: 127,
    width: 306,
    shadowOpacity: 1,
    elevation: 8,
    shadowRadius: 8,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    display: "none",
    position: "absolute",
  },
  text: {
    marginLeft: -160,
    top: 13,
  },
  batteryIcon: {
    marginLeft: 12.5,
    top: 2,
    height: 10,
    left: "50%",
  },
  text1: {
    marginLeft: -19.5,
    top: 0,
  },
  frame1: {
    left: 301,
    width: 39,
    height: 14,
    top: 13,
    overflow: "hidden",
    position: "absolute",
  },
  topBar: {
    height: 33,
    left: "50%",
    top: 0,
    position: "absolute",
    backgroundColor: Color.neutralLightLightest,
  },
  frameItem: {
    marginLeft: -3.5,
  },
  frameInner: {
    marginLeft: 2.5,
  },
  frame2: {
    height: 14,
    overflow: "hidden",
    top: 0,
    left: 0,
  },
  vectorIcon: {
    marginLeft: 91.52,
    width: 8,
    height: 14,
  },
  rectangleView: {
    marginLeft: -50.75,
  },
  frameChild1: {
    marginLeft: 36.75,
    borderRadius: Border.br_9xs,
    borderColor: Color.colorGray_200,
    width: 14,
    height: 14,
    borderWidth: 1,
    borderStyle: "solid",
    bottom: 0,
  },
  frame4: {
    width: 102,
    height: 14,
    overflow: "hidden",
    top: 0,
    left: 0,
    position: "absolute",
  },
  frame3: {
    left: 12,
    width: 200,
    overflow: "hidden",
    top: 0,
  },
  frameParent: {
    top: 16,
    left: 74,
    width: 212,
  },
  homeInner: {
    backgroundColor: Color.colorWhitesmoke_200,
    height: 45,
    marginLeft: -180,
    width: 360,
  },
  spotlessFasterAnd: {
    top: 42,
  },
  generalCleaning: {
    height: 38,
    width: 112
  },
  spotlessFasterAndHassleFParent: {
    height: 66,
    width: 112,
    left: 17,
    top: 129,
    position: "absolute",
  },
  rectangleLineargradient: {
    backgroundColor: "transparent",
  },
  frame6: {
    overflow: "hidden",
    top: 0,
    left: 0,
    position: "absolute"
  },
  frameView: {
    shadowColor: "rgba(221, 125, 225, 0.5)",
    width: 146,
    shadowOpacity: 1,
    elevation: 8,
    shadowRadius: 8,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    marginTop: -20
  },
  tacklesDirtStains: {
    top: 41,
  },
  deepCleaning: {
    width: 108,
    height: 33,
  },
  tacklesDirtStainsAndBactParent: {
    left: 15
  },
  frameParentShadowBox: {
    marginTop: -20,
    marginLeft: 12,
    height: 214,
    width: 146,
    shadowColor: "rgba(221, 125, 225, 0.5)",
    shadowOpacity: 1,
    elevation: 8,
    shadowRadius: 8,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
  revitalizeMarbleAnd: {
    top: 60,
  },
  marbleGraniteContainer: {
    height: 56,
    width: 112,
  },
  revitalizeMarbleAndGraniteParent: {
    height: 84,
  },
  carWash: {
    width: 112,
    height: 33,
  },
  getYourCarGleamingLikeNewParent: {
    left: 17,
  },
  fromAirConditioners: {
    top: 59,
  },
  electronicCleaningAnd: {
    height: 51,
    width: 112,
  },
  fromAirConditionersToStoveParent: {
    height: 83,
  },
  frameContainer: {
    marginLeft: -152,
    top: 367,
    width: 304,
    flexDirection: "row",
    flexWrap: "wrap",
    left: "50%",
    position: "absolute",
  },
  whatAreYou: {
    fontWeight: "600",
    fontFamily: FontFamily.actionActionM,
    fontSize: FontSize.size_base,
    color: Color.gBoard000000,
    textAlign: "left",
    top: 0,
    left: 0,
    position: "absolute",
  },
  frame11: {
    top: 330,
    left: 29,
    width: 302,
    height: 19,
    overflow: "hidden",
    position: "absolute",
    marginTop: -10
  },
  frame5: {
    top: 98,
    height: 1259,
    overflow: "hidden",
  },
  frameGroup: {
    top: 55,
    height: 1357,
  },
  home: {
    flex: 1,
    width: "100%",
    height: 1554,
    backgroundColor: Color.neutralLightLightest,
  },
});

export default HOME;
