import * as React from "react";
import { StyleSheet, View, Pressable, Text } from "react-native";
import { Image } from "expo-image";
import { useNavigation } from "@react-navigation/native";
import SectionCard from "../components/SectionCard";
import { Color, Padding, Border, FontSize, FontFamily } from "../GlobalStyles";

const ChatPage1 = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.chatPage}>
      <Pressable
        style={styles.chatPageChild}
        onPress={() => navigation.navigate("ChatPage2")}
      />
      <View style={[styles.chatPageItem, styles.groupChildPosition]} />
      <View style={[styles.chatPageInner, styles.navBarPosition]} />
      <View style={styles.sideGuide}>
        <View style={[styles.sideGuideChild, styles.sideLayout]} />
        <View style={[styles.sideGuideItem, styles.sideLayout]} />
      </View>
      <View style={styles.rectangleView} />
      <View style={[styles.messageBubbleParent, styles.groupParentFlexBox]}>
        <View style={[styles.messageBubble, styles.messageSpaceBlock]}>
          <Text style={[styles.name, styles.nameTypo]}>Brooke</Text>
          <Text style={[styles.message, styles.messageTypo]}>Hey Oswin!</Text>
        </View>
        <Text style={styles.text}>08:00</Text>
      </View>
      <View style={[styles.messageBubbleGroup, styles.groupParentFlexBox]}>
        <View style={[styles.messageBubble1, styles.messagePosition]}>
          <Text style={[styles.name, styles.nameTypo]}>Name</Text>
          <Text
            style={[styles.message, styles.messageTypo]}
          >{`Thank you for ordering Kilapin! I will
be there in 15 minutes.`}</Text>
        </View>
        <Text style={styles.text}>08:00</Text>
      </View>
      <View style={[styles.parent, styles.parentFlexBox]}>
        <Text style={styles.text2}>08:00</Text>
        <View style={[styles.messageBubble2, styles.messagePosition]}>
          <Text style={[styles.name2, styles.nameTypo]}>Name</Text>
          <Text style={[styles.message2, styles.messageTypo]}>
            Sudah di mana?
          </Text>
        </View>
      </View>
      <View style={[styles.messageBubbleContainer, styles.groupParentFlexBox]}>
        <View style={[styles.messageBubble1, styles.messagePosition]}>
          <Text style={[styles.name, styles.nameTypo]}>Brooke</Text>
          <Text style={[styles.message, styles.messageTypo]}>
            Halo, saya belum sampai
          </Text>
        </View>
        <Text style={styles.text}>08:00</Text>
      </View>
      <View style={[styles.group, styles.groupParentFlexBox]}>
        <Text style={styles.text2}>08:00</Text>
        <View style={[styles.messageBubble4, styles.messageSpaceBlock]}>
          <Text style={[styles.name2, styles.nameTypo]}>Lucas</Text>
          <Text style={[styles.message2, styles.messageTypo]}>Halo Kevin</Text>
        </View>
      </View>
      <Text style={[styles.today, styles.todayFlexBox]}>TODAY</Text>
      <View style={styles.chatPageChild1} />
      <View style={[styles.navBar, styles.navBarPosition]}>
        <Text style={[styles.pageTitle, styles.pageTitlePosition]}>
          Kevin Koopman
        </Text>
        <View style={[styles.leftButton, styles.pageTitlePosition]}>
          <View style={styles.fill} />
        </View>
        <Image
          style={[styles.avatarIcon, styles.pageTitlePosition]}
          contentFit="cover"
          source={require("../assets/avatar.png")}
        />
      </View>
      <View style={styles.groupChildPosition}>
        <View style={[styles.groupChild, styles.groupChildPosition]} />
        <Text style={[styles.text5, styles.textClr]}>100%</Text>
        <Text style={[styles.text6, styles.textClr]}>00:00</Text>
        <Image
          style={styles.batteryIcon}
          contentFit="cover"
          source={require("../assets/battery-icon.png")}
        />
      </View>
      <View style={styles.frameParent}>
        <SectionCard />
        <View style={styles.groupPosition}>
          <View style={[styles.groupItem, styles.groupPosition]} />
          <View style={[styles.vectorParent, styles.groupInnerLayout]}>
            <Image
              style={[styles.groupInner, styles.groupInnerLayout]}
              contentFit="cover"
              source={require("../assets/vector-1.png")}
            />
            <View style={styles.groupChild1} />
            <View style={[styles.groupChild2, styles.groupChildLayout]} />
            <View style={[styles.groupChild3, styles.groupChildLayout]} />
            <View style={[styles.groupChild4, styles.groupChildLayout]} />
          </View>
        </View>
        <View style={styles.keyboard}>
          <View style={styles.keyboard1}>
            <View style={[styles.componentBar, styles.componentBarFlexBox]}>
              <View
                style={[styles.componentButton, styles.componentSpaceBlock1]}
              >
                <Image
                  style={[styles.iconMaterialIconChevron, styles.iconLayout]}
                  contentFit="cover"
                  source={require("../assets/icon--materialicon--chevron-left-black-24dp.png")}
                />
              </View>
              <View style={[styles.cnt, styles.cntFlexBox1]}>
                <Text style={styles.textTypo1} numberOfLines={16}>
                  Suggest
                </Text>
                <Image
                  style={styles.separatorIcon}
                  contentFit="cover"
                  source={require("../assets/separator.png")}
                />
                <Text
                  style={[styles.text8, styles.textTypo1]}
                  numberOfLines={16}
                >
                  Suggest
                </Text>
                <Image
                  style={styles.separatorIcon}
                  contentFit="cover"
                  source={require("../assets/separator.png")}
                />
                <Text
                  style={[styles.text8, styles.textTypo1]}
                  numberOfLines={16}
                >
                  Suggest
                </Text>
              </View>
              <View style={[styles.componentButton1, styles.cntFlexBox1]}>
                <Image
                  style={[styles.iconMaterialIconChevron, styles.iconLayout]}
                  contentFit="cover"
                  source={require("../assets/icon--materialicon--mic-black-24dp.png")}
                />
              </View>
            </View>
            <View
              style={[
                styles.keysLayoutAlphabeticEng,
                styles.componentBarFlexBox,
              ]}
            >
              <View style={styles.cnt1}>
                <View style={[styles.componentKey, styles.componentSpaceBlock]}>
                  <Text style={[styles.text10, styles.textTypo2]}>Q</Text>
                  <Text style={styles.superscriptPosition}>1</Text>
                </View>
                <View
                  style={[styles.componentKey1, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>W</Text>
                  <Text style={styles.superscriptPosition}>2</Text>
                </View>
                <View
                  style={[styles.componentKey2, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>E</Text>
                  <Text style={styles.superscriptPosition}>3</Text>
                </View>
                <View
                  style={[styles.componentKey3, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>R</Text>
                  <Text style={styles.superscriptPosition}>4</Text>
                </View>
                <View
                  style={[styles.componentKey4, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>T</Text>
                  <Text style={styles.superscriptPosition}>5</Text>
                </View>
                <View
                  style={[styles.componentKey5, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>Y</Text>
                  <Text style={styles.superscriptPosition}>6</Text>
                </View>
                <View
                  style={[styles.componentKey6, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>U</Text>
                  <Text style={styles.superscriptPosition}>7</Text>
                </View>
                <View
                  style={[styles.componentKey7, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>I</Text>
                  <Text style={styles.superscriptPosition}>8</Text>
                </View>
                <View
                  style={[styles.componentKey8, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>O</Text>
                  <Text style={styles.superscriptPosition}>9</Text>
                </View>
                <View
                  style={[styles.componentKey9, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>P</Text>
                  <Text style={styles.superscriptPosition}>0</Text>
                </View>
              </View>
              <View style={[styles.cnt2, styles.cntFlexBox]}>
                <View
                  style={[styles.componentKey10, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>A</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey11, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>S</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey12, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>D</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey13, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>F</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey14, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>G</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey15, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>H</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey16, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>J</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey17, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>K</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey18, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>L</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
              </View>
              <View style={styles.cntFlexBox}>
                <View style={[styles.componentKey19, styles.componentFlexBox]}>
                  <Image
                    style={[styles.iconMaterialIconChevron, styles.iconLayout]}
                    contentFit="cover"
                    source={require("../assets/icon--materialicon--caps-lock.png")}
                  />
                </View>
                <View
                  style={[styles.componentKey20, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>Z</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey21, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>X</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey22, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>C</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey23, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>V</Text>
                  <Text style={styles.superscriptPosition}>{` `}</Text>
                </View>
                <View
                  style={[styles.componentKey24, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>B</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey25, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>N</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View
                  style={[styles.componentKey26, styles.componentSpaceBlock]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>M</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >
                    #
                  </Text>
                </View>
                <View style={[styles.componentKey27, styles.componentFlexBox]}>
                  <Image
                    style={[styles.iconMaterialIconChevron, styles.iconLayout]}
                    contentFit="cover"
                    source={require("../assets/icon--materialicon--backspace-black-24dp.png")}
                  />
                </View>
              </View>
              <View style={styles.cntFlexBox}>
                <View
                  style={[styles.componentKey28, styles.componentShadowBox1]}
                >
                  <Text style={[styles.text36, styles.textTypo]}>?123</Text>
                </View>
                <View
                  style={[styles.componentKey29, styles.componentShadowBox]}
                >
                  <Text style={[styles.text37, styles.textTypo]}>,</Text>
                  <Image
                    style={styles.iconMaterialIconSentime}
                    contentFit="cover"
                    source={require("../assets/icon--materialicon--sentiment-satisfied-black-24dp.png")}
                  />
                </View>
                <View
                  style={[styles.componentKey30, styles.componentShadowBox]}
                >
                  <Image
                    style={[styles.iconMaterialIconChevron, styles.iconLayout]}
                    contentFit="cover"
                    source={require("../assets/icon--materialicon--language-black-24dp.png")}
                  />
                </View>
                <View
                  style={[styles.componentKey31, styles.componentShadowBox1]}
                >
                  <Text style={[styles.text38, styles.textTypo2]}>English</Text>
                </View>
                <View
                  style={[styles.componentKey32, styles.componentShadowBox]}
                >
                  <Text style={[styles.text10, styles.textTypo2]}>.</Text>
                  <Text
                    style={[
                      styles.superscriptText10,
                      styles.superscriptPosition,
                    ]}
                  >{` `}</Text>
                </View>
                <View style={[styles.componentKey33, styles.componentFlexBox]}>
                  <Image
                    style={[styles.iconMaterialIconChevron, styles.iconLayout]}
                    contentFit="cover"
                    source={require("../assets/icon--materialicon--keyboard-tab-black-24dp.png")}
                  />
                </View>
              </View>
            </View>
            <View style={[styles.componentHomeIndicator, styles.parentFlexBox]}>
              <View style={styles.iconLayout}>
                <View style={styles.fill1} />
              </View>
              <Image
                style={[styles.iconHome, styles.iconLayout]}
                contentFit="cover"
                source={require("../assets/icon--home.png")}
              />
              <Image
                style={[styles.iconHome, styles.iconLayout]}
                contentFit="cover"
                source={require("../assets/icon--back.png")}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  groupChildPosition: {
    height: 33,
    top: 0,
    width: 360,
    left: 0,
    position: "absolute",
  },
  navBarPosition: {
    top: 33,
    left: 0,
    position: "absolute",
  },
  sideLayout: {
    width: 16,
    borderWidth: 1,
    borderColor: Color.colorLime,
    borderStyle: "solid",
    top: 0,
    position: "absolute",
    height: 800,
  },
  groupParentFlexBox: {
    alignItems: "flex-end",
    position: "absolute",
  },
  messageSpaceBlock: {
    paddingVertical: Padding.p_xs,
    paddingHorizontal: Padding.p_base,
    borderRadius: Border.br_xl,
  },
  nameTypo: {
    textAlign: "left",
    fontSize: FontSize.headingH5_size,
    fontFamily: FontFamily.headingH5,
    fontWeight: "700",
    display: "none",
  },
  messageTypo: {
    marginTop: 4,
    fontFamily: FontFamily.bodyBodyS,
    lineHeight: 20,
    fontSize: FontSize.bodyBodyM_size,
    textAlign: "left",
  },
  messagePosition: {
    borderTopRightRadius: Border.br_xl,
    borderTopLeftRadius: Border.br_xl,
    paddingVertical: Padding.p_xs,
    paddingHorizontal: Padding.p_base,
  },
  parentFlexBox: {
    justifyContent: "center",
    flexDirection: "row",
  },
  todayFlexBox: {
    textAlign: "center",
    left: "50%",
  },
  pageTitlePosition: {
    top: "50%",
    position: "absolute",
  },
  textClr: {
    color: Color.gBoard000000,
    position: "absolute",
  },
  groupPosition: {
    height: 45,
    bottom: 0,
    width: 360,
    left: 0,
    position: "absolute",
  },
  groupInnerLayout: {
    height: 15,
    position: "absolute",
  },
  groupChildLayout: {
    width: 1,
    backgroundColor: Color.colorGray_300,
    height: 14,
    bottom: 1,
    position: "absolute",
  },
  componentBarFlexBox: {
    backgroundColor: Color.gBoardE8EAED,
    alignItems: "center",
    alignSelf: "stretch",
  },
  componentSpaceBlock1: {
    padding: Padding.p_10xs,
    borderRadius: Border.br_11xl,
    overflow: "hidden",
  },
  iconLayout: {
    height: 24,
    width: 24,
  },
  cntFlexBox1: {
    marginLeft: 277,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  textTypo1: {
    height: 21,
    color: Color.gBoard454647,
    fontSize: FontSize.gBoardMedium_size,
    fontFamily: FontFamily.gBoardLarge,
    textAlign: "center",
    overflow: "hidden",
    flex: 1,
  },
  componentSpaceBlock: {
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  textTypo2: {
    fontFamily: FontFamily.gBoardLarge,
    color: Color.gBoard000000,
    textAlign: "center",
  },
  cntFlexBox: {
    marginTop: 10,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "stretch",
    flexDirection: "row",
  },
  superscriptPosition: {
    zIndex: 1,
    color: Color.gBoard666666,
    fontFamily: FontFamily.gBoardTiny,
    right: 2,
    top: 2,
    textAlign: "right",
    fontSize: FontSize.bodyBodyXS_size,
    fontWeight: "700",
    position: "absolute",
  },
  componentFlexBox: {
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: Padding.p_10xs,
    alignItems: "center",
    height: 40,
    justifyContent: "center",
    flexDirection: "row",
  },
  componentShadowBox1: {
    padding: Padding.p_11xs,
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    alignItems: "center",
    height: 40,
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    justifyContent: "center",
  },
  textTypo: {
    fontFamily: FontFamily.gBoardDefault,
    fontSize: FontSize.gBoardDefault_size,
    textAlign: "center",
    fontWeight: "500",
  },
  componentShadowBox: {
    width: 30,
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    marginLeft: 4,
  },
  chatPageChild: {
    top: 103,
    height: 345,
    width: 360,
    left: 0,
    position: "absolute",
    backgroundColor: Color.neutralLightLightest,
  },
  chatPageItem: {
    backgroundColor: Color.colorGainsboro_200,
  },
  chatPageInner: {
    backgroundColor: Color.colorWhitesmoke_100,
    height: 56,
    width: 360,
  },
  sideGuideChild: {
    left: 344,
  },
  sideGuideItem: {
    left: 0,
  },
  sideGuide: {
    display: "none",
    top: 0,
    width: 360,
    left: 0,
    position: "absolute",
    height: 800,
  },
  rectangleView: {
    top: 703,
    height: 52,
    display: "none",
    backgroundColor: Color.colorGainsboro_200,
    width: 360,
    left: 0,
    position: "absolute",
  },
  name: {
    color: Color.neutralDarkLight,
    alignSelf: "stretch",
  },
  message: {
    color: Color.neutralDarkDarkest,
  },
  messageBubble: {
    backgroundColor: Color.colorLavenderblush,
  },
  text: {
    marginLeft: 4,
    color: Color.neutralDarkLightest,
    lineHeight: 14,
    letterSpacing: 0.2,
    fontSize: FontSize.bodyBodyXS_size,
    fontFamily: FontFamily.bodyBodyS,
    textAlign: "left",
  },
  messageBubbleParent: {
    top: 135,
    flexDirection: "row",
    left: 15,
    alignItems: "flex-end",
  },
  messageBubble1: {
    borderBottomRightRadius: Border.br_xl,
    backgroundColor: Color.colorLavenderblush,
  },
  messageBubbleGroup: {
    top: 187,
    flexDirection: "row",
    left: 15,
    alignItems: "flex-end",
  },
  text2: {
    textAlign: "right",
    color: Color.neutralDarkLightest,
    lineHeight: 14,
    letterSpacing: 0.2,
    fontSize: FontSize.bodyBodyXS_size,
    fontFamily: FontFamily.bodyBodyS,
  },
  name2: {
    color: Color.highlightLight,
  },
  message2: {
    color: Color.neutralLightLightest,
  },
  messageBubble2: {
    borderBottomLeftRadius: Border.br_xl,
    backgroundColor: Color.colorOrchid_100,
    marginLeft: 4,
  },
  parent: {
    top: 313,
    right: 17,
    alignItems: "flex-end",
    position: "absolute",
  },
  messageBubbleContainer: {
    top: 367,
    flexDirection: "row",
    left: 15,
    alignItems: "flex-end",
  },
  messageBubble4: {
    backgroundColor: Color.colorOrchid_100,
    marginLeft: 4,
  },
  group: {
    top: 261,
    justifyContent: "flex-end",
    right: 16,
    flexDirection: "row",
  },
  today: {
    marginLeft: -17,
    top: 113,
    fontFamily: FontFamily.interMedium,
    color: Color.colorGray_300,
    fontWeight: "500",
    textAlign: "center",
    fontSize: FontSize.bodyBodyXS_size,
    position: "absolute",
  },
  chatPageChild1: {
    marginLeft: -180,
    shadowColor: "rgba(0, 0, 0, 0.07)",
    shadowRadius: 94,
    elevation: 94,
    height: 103,
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    left: "50%",
    top: 0,
    width: 360,
    position: "absolute",
    backgroundColor: Color.neutralLightLightest,
  },
  pageTitle: {
    marginTop: -8.5,
    marginLeft: -61,
    fontSize: FontSize.size_base,
    textAlign: "center",
    left: "50%",
    color: Color.neutralDarkDarkest,
    fontFamily: FontFamily.headingH5,
    fontWeight: "700",
    top: "50%",
  },
  fill: {
    height: "100%",
    top: "-0.01%",
    right: "0%",
    bottom: "0.01%",
    left: "0%",
    backgroundColor: Color.colorOrchid_100,
    position: "absolute",
    width: "100%",
  },
  leftButton: {
    marginTop: -10,
    left: 24,
    width: 20,
    height: 20,
    overflow: "hidden",
  },
  avatarIcon: {
    marginTop: -20,
    borderRadius: Border.br_base,
    width: 40,
    height: 40,
    right: 16,
    overflow: "hidden",
  },
  navBar: {
    right: 1,
    height: 70,
    overflow: "hidden",
    backgroundColor: Color.neutralLightLightest,
  },
  groupChild: {
    backgroundColor: Color.neutralLightLightest,
  },
  text5: {
    left: 301,
    fontFamily: FontFamily.arial,
    top: 13,
    color: Color.gBoard000000,
    textAlign: "left",
    fontSize: FontSize.headingH5_size,
  },
  text6: {
    left: 20,
    fontFamily: FontFamily.arial,
    top: 13,
    color: Color.gBoard000000,
    textAlign: "left",
    fontSize: FontSize.headingH5_size,
  },
  batteryIcon: {
    height: "30.77%",
    width: "1.94%",
    top: "46.15%",
    right: "5.56%",
    bottom: "23.08%",
    left: "92.5%",
    maxWidth: "100%",
    maxHeight: "100%",
    position: "absolute",
    overflow: "hidden",
  },
  groupItem: {
    backgroundColor: Color.colorWhitesmoke_200,
  },
  groupInner: {
    left: 203,
    width: 9,
    bottom: 0,
  },
  groupChild1: {
    left: 100,
    borderRadius: Border.br_9xs,
    borderColor: Color.colorGray_200,
    width: 14,
    height: 14,
    bottom: 1,
    borderWidth: 1,
    borderStyle: "solid",
    position: "absolute",
  },
  groupChild2: {
    left: 0,
  },
  groupChild3: {
    left: 6,
  },
  groupChild4: {
    left: 12,
  },
  vectorParent: {
    bottom: 15,
    left: 74,
    width: 212,
  },
  iconMaterialIconChevron: {
    overflow: "hidden",
  },
  componentButton: {
    shadowRadius: 2,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    padding: Padding.p_10xs,
    borderRadius: Border.br_11xl,
    alignItems: "center",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: Color.neutralLightLightest,
  },
  separatorIcon: {
    width: 0,
    height: 22,
    marginLeft: 4,
  },
  text8: {
    marginLeft: 4,
  },
  cnt: {
    display: "none",
    flex: 1,
  },
  componentButton1: {
    padding: Padding.p_10xs,
    borderRadius: Border.br_11xl,
    overflow: "hidden",
    marginLeft: 277,
  },
  componentBar: {
    paddingLeft: Padding.p_xs,
    paddingTop: Padding.p_6xs,
    paddingRight: Padding.p_6xs,
    paddingBottom: Padding.p_6xs,
    flexDirection: "row",
  },
  text10: {
    fontSize: FontSize.gBoardLarge_size,
    zIndex: 0,
    flex: 1,
  },
  componentKey: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
  },
  componentKey1: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey2: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey3: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey4: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey5: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey6: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey7: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey8: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey9: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  cnt1: {
    alignSelf: "stretch",
    flexDirection: "row",
  },
  superscriptText10: {
    display: "none",
  },
  componentKey10: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
  },
  componentKey11: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey12: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey13: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey14: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey15: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey16: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey17: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey18: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  cnt2: {
    paddingHorizontal: 18,
    paddingVertical: 0,
  },
  componentKey19: {
    width: 48,
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: Padding.p_10xs,
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    backgroundColor: Color.gBoardCCCED5,
  },
  componentKey20: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey21: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey22: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey23: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey24: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey25: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey26: {
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
    marginLeft: 4,
  },
  componentKey27: {
    width: 48,
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: Padding.p_10xs,
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    backgroundColor: Color.gBoardCCCED5,
    marginLeft: 4,
  },
  text36: {
    color: Color.gBoard3D3D3F,
    alignSelf: "stretch",
  },
  componentKey28: {
    width: 53,
    backgroundColor: Color.gBoardCCCED5,
  },
  text37: {
    top: 18,
    left: 2,
    width: 26,
    color: Color.gBoard000000,
    position: "absolute",
  },
  iconMaterialIconSentime: {
    marginLeft: -7,
    top: 5,
    width: 13,
    height: 13,
    left: "50%",
    position: "absolute",
    overflow: "hidden",
  },
  componentKey29: {
    backgroundColor: Color.gBoardCCCED5,
    height: 40,
  },
  componentKey30: {
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: Padding.p_10xs,
    alignItems: "center",
    height: 40,
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: Color.neutralLightLightest,
  },
  text38: {
    fontSize: FontSize.gBoardSmall_size,
    alignSelf: "stretch",
  },
  componentKey31: {
    marginLeft: 4,
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
  },
  componentKey32: {
    backgroundColor: Color.gBoardCCCED5,
    paddingBottom: Padding.p_8xs,
    paddingTop: Padding.p_5xs,
    paddingHorizontal: Padding.p_11xs,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  componentKey33: {
    backgroundColor: Color.gBoard1A73E8,
    width: 48,
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: Padding.p_10xs,
    elevation: 0,
    shadowRadius: 0,
    borderRadius: Border.br_7xs,
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: -331,
    },
    marginLeft: 4,
  },
  keysLayoutAlphabeticEng: {
    padding: Padding.p_9xs,
  },
  fill1: {
    height: "50%",
    width: "50%",
    top: "25%",
    right: "25%",
    bottom: "25%",
    left: "25%",
    borderRadius: 1,
    backgroundColor: Color.gBoard666666,
    position: "absolute",
  },
  iconHome: {
    marginLeft: 58,
  },
  componentHomeIndicator: {
    padding: Padding.p_2xs,
    alignItems: "center",
    alignSelf: "stretch",
    display: "none",
    overflow: "hidden",
    backgroundColor: Color.neutralLightLightest,
  },
  keyboard1: {
    height: 242,
    alignItems: "center",
    width: 360,
  },
  keyboard: {
    bottom: 45,
    left: 0,
    position: "absolute",
  },
  frameParent: {
    right: 0,
    height: 355,
    bottom: 0,
    left: 0,
    position: "absolute",
  },
  chatPage: {
    overflow: "hidden",
    height: 800,
    width: "100%",
    flex: 1,
    backgroundColor: Color.neutralLightLightest,
  },
});

export default ChatPage1;
