import * as React from "react";
import { StyleSheet, View, Text, Pressable } from "react-native";
import { Image } from "expo-image";
import { useNavigation } from "@react-navigation/native";
import SectionForm1 from "../components/SectionForm1";
import { Color, Padding, Border, FontFamily, FontSize } from "../GlobalStyles";

const ChatPage2 = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.chatPage}>
      <View style={[styles.chatPageChild, styles.childPosition]} />
      <View style={[styles.chatPageItem, styles.navBarPosition]} />
      <View style={[styles.sideGuide, styles.sideGuidePosition]}>
        <View style={[styles.sideGuideChild, styles.sideLayout]} />
        <View style={[styles.sideGuideItem, styles.sideLayout]} />
      </View>
      <View style={[styles.chatPageInner, styles.sideGuidePosition]} />
      <View style={[styles.messageBubbleParent, styles.groupParentFlexBox]}>
        <View style={[styles.messageBubble, styles.messageSpaceBlock]}>
          <Text style={[styles.name, styles.nameTypo]}>Brooke</Text>
          <Text style={[styles.message, styles.messageTypo]}>Hey Oswin!</Text>
        </View>
        <Text style={[styles.text, styles.textTypo1]}>08:00</Text>
      </View>
      <View style={[styles.messageBubbleGroup, styles.groupParentFlexBox]}>
        <View style={[styles.messageBubble1, styles.messagePosition]}>
          <Text style={[styles.name, styles.nameTypo]}>Name</Text>
          <Text
            style={[styles.message, styles.messageTypo]}
          >{`Thank you for ordering Kilapin! I will
be there in 15 minutes.`}</Text>
        </View>
        <Text style={[styles.text, styles.textTypo1]}>08:00</Text>
      </View>
      <View style={[styles.parent, styles.groupParentFlexBox]}>
        <Text style={[styles.text2, styles.textTypo1]}>08:00</Text>
        <View style={[styles.messageBubble2, styles.messagePosition]}>
          <Text style={[styles.name2, styles.nameTypo]}>Name</Text>
          <Text style={[styles.message2, styles.messageTypo]}>
            Sudah di mana?
          </Text>
        </View>
      </View>
      <View style={[styles.messageBubbleContainer, styles.groupParentFlexBox]}>
        <View style={[styles.messageBubble1, styles.messagePosition]}>
          <Text style={[styles.name, styles.nameTypo]}>Brooke</Text>
          <Text style={[styles.message, styles.messageTypo]}>
            Halo, saya belum sampai
          </Text>
        </View>
        <Text style={[styles.text, styles.textTypo1]}>08:00</Text>
      </View>
      <View style={[styles.group, styles.groupParentFlexBox]}>
        <Text style={[styles.text2, styles.textTypo1]}>08:00</Text>
        <View style={[styles.messageBubble4, styles.messageSpaceBlock]}>
          <Text style={[styles.name2, styles.nameTypo]}>Lucas</Text>
          <Text style={[styles.message2, styles.messageTypo]}>Halo Kevin</Text>
        </View>
      </View>
      <Text style={[styles.today, styles.todayFlexBox]}>TODAY</Text>
      <View style={styles.rectangleView} />
      <View style={[styles.navBar, styles.navBarPosition]}>
        <Text style={[styles.pageTitle, styles.pageTitlePosition]}>
          Kevin Koopman
        </Text>
        <Pressable
          style={[styles.leftButton, styles.pageTitlePosition]}
          onPress={() => navigation.navigate("Chat")}
        >
          <View style={styles.fill} />
        </Pressable>
        <Image
          style={[styles.avatarIcon, styles.pageTitlePosition]}
          contentFit="cover"
          source={require("../assets/avatar.png")}
        />
      </View>
      <View style={styles.childPosition}>
        <View style={[styles.groupChild, styles.childPosition]} />
        <Text style={[styles.text5, styles.textTypo]}>100%</Text>
        <Text style={[styles.text6, styles.textTypo]}>00:00</Text>
        <Image
          style={styles.batteryIcon}
          contentFit="cover"
          source={require("../assets/battery-icon.png")}
        />
      </View>
      <SectionForm1 />
    </View>
  );
};

const styles = StyleSheet.create({
  childPosition: {
    height: 33,
    width: 360,
    left: 0,
    top: 0,
    position: "absolute",
  },
  navBarPosition: {
    top: 33,
    left: 0,
    position: "absolute",
  },
  sideGuidePosition: {
    display: "none",
    width: 360,
    left: 0,
    position: "absolute",
  },
  sideLayout: {
    width: 16,
    borderWidth: 1,
    borderColor: Color.colorLime,
    borderStyle: "solid",
    top: 0,
    position: "absolute",
    height: 800,
  },
  groupParentFlexBox: {
    alignItems: "flex-end",
    flexDirection: "row",
    position: "absolute",
  },
  messageSpaceBlock: {
    paddingVertical: Padding.p_xs,
    paddingHorizontal: Padding.p_base,
    borderRadius: Border.br_xl,
  },
  nameTypo: {
    fontFamily: FontFamily.headingH5,
    fontWeight: "700",
  },
  messageTypo: {
    marginTop: 4,
    fontFamily: FontFamily.bodyBodyS,
    lineHeight: 20,
    fontSize: FontSize.bodyBodyM_size,
    textAlign: "left",
  },
  textTypo1: {
    color: Color.neutralDarkLightest,
    lineHeight: 14,
    letterSpacing: 0.2,
    fontSize: FontSize.bodyBodyXS_size,
    fontFamily: FontFamily.bodyBodyS,
  },
  messagePosition: {
    borderTopRightRadius: Border.br_xl,
    borderTopLeftRadius: Border.br_xl,
    paddingVertical: Padding.p_xs,
    paddingHorizontal: Padding.p_base,
  },
  todayFlexBox: {
    textAlign: "center",
    left: "50%",
  },
  pageTitlePosition: {
    top: "50%",
    position: "absolute",
  },
  textTypo: {
    color: Color.gBoard000000,
    fontFamily: FontFamily.arial,
    top: 13,
    textAlign: "left",
    fontSize: FontSize.headingH5_size,
    position: "absolute",
  },
  chatPageChild: {
    backgroundColor: Color.colorGainsboro_200,
  },
  chatPageItem: {
    backgroundColor: Color.colorWhitesmoke_100,
    height: 56,
    width: 360,
    top: 33,
  },
  sideGuideChild: {
    left: 344,
  },
  sideGuideItem: {
    left: 0,
    borderWidth: 1,
    borderColor: Color.colorLime,
    borderStyle: "solid",
  },
  sideGuide: {
    top: 0,
    display: "none",
    height: 800,
  },
  chatPageInner: {
    top: 703,
    height: 52,
    backgroundColor: Color.colorGainsboro_200,
  },
  name: {
    alignSelf: "stretch",
    color: Color.neutralDarkLight,
    textAlign: "left",
    fontSize: FontSize.headingH5_size,
    fontFamily: FontFamily.headingH5,
    fontWeight: "700",
    display: "none",
  },
  message: {
    color: Color.neutralDarkDarkest,
  },
  messageBubble: {
    backgroundColor: Color.colorLavenderblush,
  },
  text: {
    marginLeft: 4,
    textAlign: "left",
  },
  messageBubbleParent: {
    top: 135,
    left: 15,
    flexDirection: "row",
  },
  messageBubble1: {
    borderBottomRightRadius: Border.br_xl,
    backgroundColor: Color.colorLavenderblush,
  },
  messageBubbleGroup: {
    top: 187,
    left: 15,
    flexDirection: "row",
  },
  text2: {
    textAlign: "right",
  },
  name2: {
    color: Color.highlightLight,
    textAlign: "left",
    fontSize: FontSize.headingH5_size,
    fontFamily: FontFamily.headingH5,
    fontWeight: "700",
    display: "none",
  },
  message2: {
    color: Color.neutralLightLightest,
  },
  messageBubble2: {
    borderBottomLeftRadius: Border.br_xl,
    backgroundColor: Color.colorOrchid_100,
    marginLeft: 4,
  },
  parent: {
    top: 313,
    right: 17,
    justifyContent: "center",
  },
  messageBubbleContainer: {
    top: 367,
    left: 15,
    flexDirection: "row",
  },
  messageBubble4: {
    backgroundColor: Color.colorOrchid_100,
    marginLeft: 4,
  },
  group: {
    top: 261,
    justifyContent: "flex-end",
    right: 16,
  },
  today: {
    marginLeft: -17,
    top: 113,
    fontWeight: "500",
    fontFamily: FontFamily.interMedium,
    color: Color.colorGray_300,
    fontSize: FontSize.bodyBodyXS_size,
    textAlign: "center",
    position: "absolute",
  },
  rectangleView: {
    marginLeft: -180,
    shadowColor: "rgba(0, 0, 0, 0.07)",
    shadowOffset: {
      width: 0,
      height: -331,
    },
    shadowRadius: 94,
    elevation: 94,
    shadowOpacity: 1,
    height: 103,
    left: "50%",
    width: 360,
    top: 0,
    position: "absolute",
    backgroundColor: Color.neutralLightLightest,
  },
  pageTitle: {
    marginTop: -8.5,
    marginLeft: -61,
    fontSize: FontSize.size_base,
    textAlign: "center",
    left: "50%",
    color: Color.neutralDarkDarkest,
    fontFamily: FontFamily.headingH5,
    fontWeight: "700",
  },
  fill: {
    height: "100%",
    top: "-0.01%",
    right: "0%",
    bottom: "0.01%",
    left: "0%",
    backgroundColor: Color.colorOrchid_100,
    position: "absolute",
    width: "100%",
  },
  leftButton: {
    marginTop: -10,
    left: 24,
    width: 20,
    height: 20,
    overflow: "hidden",
  },
  avatarIcon: {
    marginTop: -20,
    borderRadius: Border.br_base,
    width: 40,
    height: 40,
    right: 16,
    overflow: "hidden",
  },
  navBar: {
    right: 1,
    height: 70,
    overflow: "hidden",
    backgroundColor: Color.neutralLightLightest,
  },
  groupChild: {
    backgroundColor: Color.neutralLightLightest,
  },
  text5: {
    left: 301,
  },
  text6: {
    left: 20,
  },
  batteryIcon: {
    height: "30.77%",
    width: "1.94%",
    top: "46.15%",
    right: "5.56%",
    bottom: "23.08%",
    left: "92.5%",
    maxWidth: "100%",
    maxHeight: "100%",
    position: "absolute",
    overflow: "hidden",
  },
  chatPage: {
    flex: 1,
    overflow: "hidden",
    height: 800,
    width: "100%",
    backgroundColor: Color.neutralLightLightest,
  },
});

export default ChatPage2;
