import * as React from "react";
import { StyleSheet, View, Text } from "react-native";
import { Image } from "expo-image";
import NewsCard from "../components/NewsCard";
import PromoCardForm from "../components/PromoCardForm";
import NewsPromoSection from "../components/NewsPromoSection";
import { Color, FontFamily, FontSize, Border } from "../GlobalStyles";

const NewsPromo = () => {
  return (
    <View style={styles.newspromo}>
      <View style={styles.newspromoChild} />
      <View style={[styles.newspromoItem, styles.newspromoPosition]} />
      <View style={[styles.newspromoInner, styles.rectangleViewLayout]} />
      <View style={[styles.rectangleView, styles.rectangleViewLayout]} />
      <Image
        style={[styles.batteryIcon, styles.iconLayout]}
        contentFit="cover"
        source={require("../assets/battery-icon.png")}
      />
      <View style={[styles.newspromoChild1, styles.newspromoPosition]} />
      <Text
        style={[styles.newsPromo, styles.newsPromoFlexBox]}
      >{`News & Promo`}</Text>
      <Text style={[styles.whatsNew, styles.whatsNewTypo]}>What’s New</Text>
      <Text style={[styles.promotions, styles.whatsNewTypo]}>Promotions</Text>
      <Text style={styles.tuesday16August}>Tuesday, 16 August</Text>
      <NewsCard />
      <PromoCardForm
        rectangle63={require("../assets/rectangle-63.png")}
        bestDealWithF45TKILAPDisc="Best Deal with F45T KILAP, Discount 45%"
      />
      <PromoCardForm
        rectangle63={require("../assets/rectangle-631.png")}
        bestDealWithF45TKILAPDisc="Special for You New Customers!"
        propLeft={179}
        propRight="-79.59%"
        propLeft1="163.27%"
      />
      <View style={[styles.promo, styles.promoLayout]}>
        <View style={[styles.promoChild, styles.promoLayout]} />
        <View style={styles.promoItem} />
        <Image
          style={[styles.vectorIcon, styles.iconLayout]}
          contentFit="cover"
          source={require("../assets/vector.png")}
        />
        <Text style={[styles.bestDealWith, styles.newsPromoFlexBox]}>
          Best Deal with F45T KILAP, Discount 45%
        </Text>
      </View>
      <NewsCard propLeft={344} />
      <View style={[styles.lineView, styles.lineViewPosition]} />
      <View style={styles.topPosition}>
        <View style={[styles.topBarChild, styles.topPosition]} />
        <Text style={[styles.text, styles.textTypo]}>100%</Text>
        <Text style={[styles.text1, styles.textTypo]}>00:00</Text>
        <Image
          style={[styles.batteryIcon1, styles.lineViewPosition]}
          contentFit="cover"
          source={require("../assets/battery-icon1.png")}
        />
      </View>
      <NewsPromoSection />
    </View>
  );
};

const styles = StyleSheet.create({
  newspromoPosition: {
    width: 360,
    left: 0,
    position: "absolute",
  },
  rectangleViewLayout: {
    width: 16,
    borderWidth: 1,
    borderColor: Color.colorLime,
    borderStyle: "solid",
    display: "none",
    top: 0,
    position: "absolute",
    height: 800,
  },
  iconLayout: {
    maxHeight: "100%",
    maxWidth: "100%",
    position: "absolute",
    overflow: "hidden",
  },
  newsPromoFlexBox: {
    textAlign: "left",
    color: Color.gBoard000000,
    position: "absolute",
  },
  whatsNewTypo: {
    fontFamily: FontFamily.headingH5,
    fontWeight: "700",
    marginLeft: -157,
    textAlign: "left",
    color: Color.gBoard000000,
    lineHeight: 18,
    fontSize: FontSize.size_xl,
    left: "50%",
    position: "absolute",
  },
  promoLayout: {
    height: 170,
    width: 147,
    position: "absolute",
  },
  lineViewPosition: {
    left: "50%",
    position: "absolute",
  },
  topPosition: {
    marginLeft: -180,
    left: "50%",
    height: 33,
    width: 360,
    top: 0,
    position: "absolute",
  },
  textTypo: {
    fontFamily: FontFamily.arial,
    top: 13,
    fontSize: FontSize.headingH5_size,
    textAlign: "left",
    color: Color.gBoard000000,
    left: "50%",
    position: "absolute",
  },
  newspromoChild: {
    display: "none",
    height: 33,
    width: 360,
    backgroundColor: Color.colorGainsboro_200,
    left: 0,
    top: 0,
    position: "absolute",
  },
  newspromoItem: {
    top: 33,
    height: 56,
  },
  newspromoInner: {
    left: 344,
  },
  rectangleView: {
    left: 0,
  },
  batteryIcon: {
    height: "1.25%",
    width: "1.94%",
    top: "1.88%",
    right: "5.56%",
    bottom: "96.88%",
    left: "92.5%",
  },
  newspromoChild1: {
    top: 703,
    height: 52,
  },
  newsPromo: {
    marginLeft: -72,
    top: 61,
    fontFamily: FontFamily.arialRoundedMTBold,
    lineHeight: 18,
    fontSize: FontSize.size_xl,
    textAlign: "left",
    color: Color.gBoard000000,
    left: "50%",
  },
  whatsNew: {
    top: 125,
  },
  promotions: {
    top: 459,
  },
  tuesday16August: {
    top: 110,
    left: 23,
    lineHeight: 11,
    fontFamily: FontFamily.bodyBodyS,
    color: Color.colorGray_300,
    fontSize: FontSize.headingH5_size,
    textAlign: "left",
    position: "absolute",
  },
  promoChild: {
    borderRadius: Border.br_xl,
    backgroundColor: Color.neutralLightLightest,
    left: 0,
    top: 0,
    height: 170,
    width: 147,
  },
  promoItem: {
    top: 6,
    left: 6,
    borderRadius: Border.br_mini,
    width: 134,
    height: 93,
    backgroundColor: Color.colorGainsboro_200,
    position: "absolute",
  },
  vectorIcon: {
    height: "14.12%",
    width: "16.33%",
    top: "310%",
    right: "-191.84%",
    bottom: "-224.12%",
    left: "275.51%",
  },
  bestDealWith: {
    top: 111,
    left: 14,
    fontSize: FontSize.bodyBodyM_size,
    lineHeight: 14,
    fontWeight: "600",
    fontFamily: FontFamily.gBoardTiny,
    width: 119,
    height: 39,
  },
  promo: {
    top: 486,
    shadowColor: "rgba(0, 0, 0, 0.03)",
    shadowOffset: {
      width: 0,
      height: 38,
    },
    shadowRadius: 145,
    elevation: 145,
    shadowOpacity: 1,
    left: 344,
  },
  lineView: {
    marginLeft: -180.5,
    top: 97,
    borderColor: "#c5c6cc",
    borderTopWidth: 1,
    width: 361,
    height: 1,
    borderStyle: "solid",
    left: "50%",
    display: "none",
  },
  topBarChild: {
    backgroundColor: Color.neutralLightLightest,
  },
  text: {
    marginLeft: 121,
  },
  text1: {
    marginLeft: -160,
  },
  batteryIcon1: {
    marginLeft: 153,
    top: 15,
    width: 7,
    height: 10,
  },
  newspromo: {
    backgroundColor: Color.colorGray_100,
    flex: 1,
    width: "100%",
    overflow: "hidden",
    height: 800,
  },
});

export default NewsPromo;
